const gulp = require("gulp");
const { parallel, series } = require("gulp");

const imagemin = require("gulp-imagemin");
const htmlmin = require("gulp-htmlmin");
const htmlReplace = require('gulp-html-replace');
const uglify = require("gulp-uglify");
const sass = require('gulp-sass')(require('sass'));
const concat = require("gulp-concat");
const browserSync = require("browser-sync").create(); //https://browsersync.io/docs/gulp#page-top
const nunjucksRender = require("gulp-nunjucks-render");
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
const order = require('gulp-order');

// /*
// TOP LEVEL FUNCTIONS
//     gulp.task = Define tasks
//     gulp.src = Point to files to use
//     gulp.dest = Points to the folder to output
//     gulp.watch = Watch files and folders for changes
// */

// Optimise Images
function imageMin(cb) {
    gulp.src("src/assets/images/*")
        .pipe(imagemin())
        .pipe(gulp.dest("public/images"));
    cb();
}



// Copy all HTML files to Dist
// function copyHTML(cb) {
//     gulp.src("src/*.html").pipe(gulp.dest("dist"));
//     cb();
// }

// Minify HTML
function minifyHTML(cb) {
    gulp.src("src/*.html")
        .pipe(gulp.dest("./"))
        .pipe(
            htmlmin({
                collapseWhitespace: true
            })
        )
        .pipe(gulp.dest("./"));
    cb();
}

// Fonts
function fonts(cb) {
    gulp.src("src/assets/fonts/*.ttf")
        .pipe(gulp.dest("public/fonts"));
    cb();
}

// Scripts
function js(cb) {
    gulp.src("src/assets/js/main/*.js")
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(concat("main.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest("public/js"));
    cb();
}
// Vendor Scripts
function vendorjs(cb) {
    gulp.src("src/assets/js/vendor/*.js")
        .pipe(order([
            "src/assets/js/vendor/jquery.js",
            "src/assets/js/vendor/popper.js",
            "src/assets/js/vendor/bootstrap.js",
            "src/assets/js/vendor/feather.js",
            "src/assets/js/vendor/highcharts.js",
            "src/assets/js/vendor/d3.v3.min.js",
            "src/assets/js/vendor/topojson.v1.min.js",
            "src/assets/js/vendor/datamaps.none.js",
            "src/assets/js/vendor/jquery.dataTables.js",
            "src/assets/js/vendor/dataTables.colReorder.min.js",
            "src/assets/js/vendor/simplebar.js"
          ], { base: './' }))
        .pipe(concat("vendor.js"))
        .pipe(gulp.dest("public/vendor"));
    cb();
}

function chartjs(cb) {
    gulp.src("src/assets/js/charts/*.js")
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(concat("charts.js"))
        .pipe(uglify())
        .pipe(gulp.dest("public/js"))
        .pipe(browserSync.stream());
    cb();
}

function tablejs(cb) {
    gulp.src("src/assets/js/tables/*.js")
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(concat("tables.js"))
        .pipe(uglify())
        .pipe(gulp.dest("public/js"))
        .pipe(browserSync.stream());
    cb();
}

// function chartjs(cb) {
//     gulp.src("src/assets/js/charts/*.js")
//         .pipe(order([
//             "src/assets/js/charts/highcharts.js"
//           ], { base: './' }))
//         .pipe(concat("charts.js"))
//         .pipe(gulp.dest("public/js"));
//     cb();
// }

// Css vendor
function vendorcss(cb) {
    gulp.src("src/assets/vendor/*.css")
        .pipe(concat("vendor.css"))
        .pipe(gulp.dest("public/css"))
        // Stream changes to all browsers
        .pipe(browserSync.stream());
    cb();
}


// Compile Sass
function css(cb) {
    gulp.src("src/assets/sass/*.scss")
        // .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(sass({ outputStyle: "expanded" }).on("error", sass.logError))
        .pipe(autoprefixer({
            browserlist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest("src/assets/css"))
        .pipe(gulp.dest("public/css"))
        // Stream changes to all browsers
        .pipe(browserSync.stream());
    cb();
}

// Process Nunjucks
function nunjucks(cb) {
    gulp.src("src/pages/*.html")
        .pipe(
            nunjucksRender({
                path: ["src/templates/"] // String or Array
            })
        )
        .pipe(gulp.dest("./"))
    cb();
}

function html() {
    return gulp.src("src/*.html")
        .pipe(htmlReplace({
            'corecss': "assets/css/app.css",
            'corejs': "assets/js/main.min.js",
            'vendorcss': "assets/css/vendor.css",
            'vendorjs': "assets/js/vendor.js",
            'chartjs': "assets/js/charts.js"
        }))
        /* .pipe(htmlMin({
            sortAttributes: true,
            sortClassName: true,
            collapseWhitespace: false
        })) */
        .pipe(gulp.dest("dist"))
}



// function nunjucksMinify(cb) {
//     gulp.src("src/pages/*.html")
//         .pipe(
//             nunjucksRender({
//                 path: ["src/templates/"] // String or Array
//             })
//         )
//         .pipe(
//             htmlmin({
//                 collapseWhitespace: true
//             })
//         )
//         .pipe(gulp.dest("dist"));
//     cb();
// }

// Watch Files
function watch_files() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("src/assets/sass/**/*.scss", css);
    gulp.watch("src/assets/js/main/*.js", js).on("change", browserSync.reload);
    gulp.watch("src/assets/js/charts/*.js", js).on("change", browserSync.reload);
    gulp.watch("src/assets/js/tables/*.js", js).on("change", browserSync.reload);
    gulp.watch("src/pages/*.html", nunjucks).on("change", browserSync.reload);
    gulp.watch("src/templates/*.html", nunjucks).on(
        "change",
        browserSync.reload
    );
}

// Default 'gulp' command with start local server and watch files for changes.
exports.default = series(nunjucks, vendorjs, chartjs, tablejs, vendorcss, fonts, css, js, watch_files);

// 'gulp build' will build all assets but not run on a local server.
exports.build = parallel(series(nunjucks, html), vendorjs, chartjs, tablejs, fonts, vendorcss, css, js, imageMin);
