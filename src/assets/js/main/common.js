class Auth {
    constructor() {
        document.querySelector("body").style.display = "none";
        const auth = localStorage.getItem("auth");
        this.validateAuth(auth);
    }

    validateAuth(auth) {
        if (auth != 1) {
            window.location.replace("https://think.design/workspace/tcs-digitate-code/");
        } else {
            document.querySelector("body").style.display = "block";
        }
    }

    logOut() {
        localStorage.removeItem("auth");
        window.location.replace("https://think.design/workspace/tcs-digitate-code/");
    }
}

const auth = new Auth();

//   document.querySelector(".logout").addEventListener("click", (e) => {
//     auth.logOut();
//   });



$(function () {


    const start = new Date(Date.now() - 24 * 60 * 60 * 1000);
    const end = new Date(Date.now() + 24 * 60 * 60 * 1000);



    if (location.hash !== null && location.hash !== "") {
        $(location.hash + ".collapse").collapse("show");

    }

    $('html, body').scrollTop(0);
    var str = window.location.href;
    if (str.indexOf('#') > -1) {
        var anchor = str.substring(str.indexOf('#'));
        setTimeout(function () {
            $('html, body').animate({ scrollTop: $(anchor).offset().top - 200 }, 'slow');
        }, 100);
    }



    var myCal = bulmaCalendar.attach('.td-date-filter', {
        lang: 'en-US',
        isRange: true,
        displayMode: 'inline',
        startDate: start,
        dateFormat: 'dd-MM-yyyy'
    });

    var myCal1 = bulmaCalendar.attach('.td-date-filter1', {
        lang: 'en-US',
        isRange: true,
        displayMode: 'inline',
        startDate: start,
        dateFormat: 'dd-MM-yyyy'
    });

    var myCal2 = bulmaCalendar.attach('.td-date-filter2', {
        lang: 'en-US',
        isRange: true,
        displayMode: 'inline',
        startDate: start,
        dateFormat: 'dd-MM-yyyy'
    });

    var myCal3 = bulmaCalendar.attach('.td-date-filter3', {
        lang: 'en-US',
        isRange: true,
        displayMode: 'inline',
        startDate: start,
        dateFormat: 'dd-MM-yyyy'
    });

    var calendar1 = bulmaCalendar.attach('.select-datepicker', {
        lang: 'en-US',
        startDate: start,
        closeOnOverlayClick: true,
        toggleOnInputClick: true,
        dateFormat: 'dd-MM-yyyy'
    });


// Initialize all input of type date
// var calendars = bulmaCalendar.attach('[type="date"]', {startDate: new Date('10/24/2019')});

// Loop on each calendar initialized

// const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
const options = { year: 'numeric', month: 'short', day: '2-digit' };
var mm = start.getMonth() + 1;
var dd = start.getDate();
var yy = start.getFullYear(); var myDateString = yy + '-' + mm + '-' + dd; console.log(myDateString)
for(var i = 0; i < myCal.length; i++) {
    // Add listener to date:selected event
    myCal[i].on('select', datepicker => {
        document.querySelector('.column-dateandtime .time-calender-wrap1 .date-stamp-wrap .date .text').innerHTML  = datepicker.data.startDate.toLocaleDateString('en-Uk', options)
        document.querySelector('.column-dateandtime .time-calender-wrap1 .second-calender-row .date-stamp-wrap .date .text').innerHTML  = datepicker.data.endDate.toLocaleDateString('en-Uk', options)
        document.querySelector('.column-dateandtime .time-calender-wrap1 .checkbox-wrap  #customnow[type=checkbox]').removeAttribute('checked')
        document.querySelector('.column-dateandtime .time-calender-wrap1 .checkbox-wrap  #customnow[type=checkbox]').checked = false;
        document.querySelector('.column-dateandtime .time-calender-wrap1 .second-calender-row ').classList.remove('disabled')

    });
}


for(var i = 0; i < myCal1.length; i++) {
    // Add listener to date:selected event
    myCal1[i].on('select', datepicker => {

        document.querySelector('.time-filter-wrap .date-stamp-wrap .date .text').innerHTML  = datepicker.data.startDate.toLocaleDateString('en-Uk', options)
        document.querySelector('.time-filter-wrap .second-calender-row .date-stamp-wrap .date .text').innerHTML  = datepicker.data.endDate.toLocaleDateString('en-Uk', options)
        document.querySelector('.time-filter-wrap .checkbox-wrap input[type=checkbox]').removeAttribute('checked')
        document.querySelector('.time-filter-wrap .checkbox-wrap input[type=checkbox]').checked = false
        document.querySelector('.time-filter-wrap .second-calender-row ').classList.remove('disabled')
    });
}

for(var i = 0; i < myCal2.length; i++) {
    // Add listener to date:selected event
    myCal2[i].on('select', datepicker => {

        document.querySelector('.column-dateandtime .time-calender-wrap2 .date-stamp-wrap .date .text').innerHTML  = datepicker.data.startDate.toLocaleDateString('en-Uk', options)
        document.querySelector('.column-dateandtime .time-calender-wrap2 .second-calender-row .date-stamp-wrap .date .text').innerHTML  = datepicker.data.endDate.toLocaleDateString('en-Uk', options)
        document.querySelector('.column-dateandtime .time-calender-wrap2 .checkbox-wrap input[type=checkbox]').removeAttribute('checked')
        document.querySelector('.column-dateandtime .time-calender-wrap2 .checkbox-wrap input[type=checkbox]').checked = false
        document.querySelector('.column-dateandtime .time-calender-wrap2 .second-calender-row ').classList.remove('disabled')
    });
}
for(var i = 0; i < myCal3.length; i++) {
    // Add listener to date:selected event
    myCal3[i].on('select', datepicker => {

        document.querySelector('.column-dateandtime .time-calender-wrap3 .date-stamp-wrap .date .text').innerHTML  = datepicker.data.startDate.toLocaleDateString('en-Uk', options)
        document.querySelector('.column-dateandtime .time-calender-wrap3 .second-calender-row .date-stamp-wrap .date .text').innerHTML  = datepicker.data.endDate.toLocaleDateString('en-Uk', options)
        document.querySelector('.column-dateandtime .time-calender-wrap3 .checkbox-wrap input[type=checkbox]').removeAttribute('checked')
        document.querySelector('.column-dateandtime .time-calender-wrap3 .checkbox-wrap input[type=checkbox]').checked = false
        document.querySelector('.column-dateandtime .time-calender-wrap3 .second-calender-row ').classList.remove('disabled')
    });
}






  

    $('.select-datepicker').on('click', function (el) {

        $(".dt-picker-wrap").removeClass("active");
        $(this).parents('.dt-picker-wrap').addClass('active')
        $('.dt-picker-wrap .datetimepicker').removeClass('is-active');
        $('.dt-picker-wrap .datetimepicker datepicker').removeClass('is-active');
        $(this).parents('.dt-picker-wrap').find('.datetimepicker').addClass('is-active')
        $(this).parents('.dt-picker-wrap').find('.datepicker').addClass('is-active')

    })




    $(document).on("click", function (e) {
        var p = $(e.target).closest('.analysis-summary-wrap .multiselect-checkbox .btn-check-wrap,   thead th a.hamburger,  thead th a.hamburger img, .column-filter, .column-dateandtime, .filter-popup-wrap, .filter-popup-event, .setting-icons-link, .setting-dropdown').length;
        if (!p) {
            $('.analysis-summary-wrap .multiselect-checkbox .select-option-wrap').removeClass('expanded-select');
            $('.analysis-summary-wrap .multiselect-checkbox .btn-check-wrap').removeClass('arrow-toggle');
            $('.column-filter').removeClass('tfilterexpand');
            $('.column-dateandtime').removeClass('tfilterexpand');
            $('.common-table-wrapper .dataTables_wrapper table > thead th').removeClass('coloredth');
            // $('#inline-datepicker').focus();
            $('.filter-popup-wrap').removeClass('expandfilter');
            $('.body-overlay').removeClass('selected');
            $('.setting-dropdown').removeClass('expand')
        }
    });

    $('.select-datepicker').on('focus', function (e) {
        $('.analysis-summary-wrap .multiselect-checkbox .select-option-wrap').removeClass('expanded-select');
        $('.analysis-summary-wrap .multiselect-checkbox .btn-check-wrap').removeClass('arrow-toggle');
        // $(this).find('.dt-picker-wrap .datetimepicker').addClass('is-active')
    })
    // $('.select-datepicker').on('focusout',function (e) {
    //     $('.dt-picker-wrap .datetimepicker').removeClass('is-active')
    // })


    $('.page-nav-steps .btn-next').on('click', function (e) {
        e.preventDefault();
        $('a[href="#none-covered-events-tab"]').tab('show');
        $('.covered-tabs-nav ul.nav-tabs li:nth-child(1) .nav-link').addClass('activated');
        $('.page-nav-steps .btn-wrap .btn-re-run, .page-nav-steps .btn-next').hide();
        $('.page-nav-steps .btn-wrap .btn-proceed').show();
    })


    $('.tag-grid-wrap ul li a').on('click', function (e) {
        var txt = $(this).find('.text').html();
        var index = 0;
        $('.pattern_txt').each(function (i) {
            index = ($(this).val() == '') ? i : i;
        });
        ($('.pattern_txt').eq(0).val() == '') ? $('.pattern_txt').eq(0).val(txt) : $('.pattern_txt').eq(index).val(txt);
    });

    $('table tr td .close-icon').on('click', function (e) {
        var rowCount = $('.common-table-wrapper .dataTables_wrapper table tbody tr').length;
        if (rowCount == 1) {
            return false;
        } else {

        }

    });



    $('.select-filter-option, .select-drop-value').select2({
        selectOnClose: true,
        minimumResultsForSearch: -1,
        containerCssClass: "error",
        dropdownCssClass: "test"
    });

    $('.table-action-modal .select-drop-value').select2({
        selectOnClose: true,
        minimumResultsForSearch: -1,
        dropdownCssClass: "table-action-modal-drop"
    });

    $('.select-wrap1').select2({
        selectOnClose: true,
        minimumResultsForSearch: -1,
        dropdownCssClass: "table-action-modal-drop"
    });

    $('.time-location-select').select2({
        selectOnClose: true,
        minimumResultsForSearch: -1,
        dropdownCssClass: "time-zoon-drop"
    });


    setTimeout(function () {
        $('.dataTables_length label select').select2({
            selectOnClose: true,
            minimumResultsForSearch: -1,
            dropdownCssClass: "table-action-length"
        })
    }, 100)



})

// Sticky top header script
const header_nav = document.querySelector('#td-top-header');
const sec_header_nav = document.querySelector('.secondary-header');
const page_header_nav = document.querySelector('.page-head-header');
const header_navTop = header_nav.clientHeight;

if (header_nav) {
    function stickyTopNav() {
        if (window.scrollY > 10) {
            header_nav.classList.add('fixed-nav');
        } else {
            header_nav.classList.remove('fixed-nav');
        }
    }
    window.addEventListener('scroll', stickyTopNav);
}


if (sec_header_nav) {
    function stickysecNav() {
        if (window.scrollY > 50) {
            sec_header_nav.classList.add('fixed-nav');
        } else {
            sec_header_nav.classList.remove('fixed-nav');
        }
    }
    window.addEventListener('scroll', stickysecNav);
}


if (page_header_nav) {
    function stickypageNav() {
        if (window.scrollY > 50) {
            page_header_nav.classList.add('fixed-nav');
        } else {
            page_header_nav.classList.remove('fixed-nav');
        }
    }
    window.addEventListener('scroll', stickypageNav);
}


// Sricky top header script


//--- GMT Time JSON Object Start ---//

var tzInts = [
    { "label": "(GMT-12:00) International Date Line West", "value": "-12" },
    { "label": "(GMT-11:00) Midway Island, Samoa", "value": "-11" },
    { "label": "(GMT-10:00) Hawaii", "value": "-10" },
    { "label": "(GMT-09:00) Alaska", "value": "-9" },
    { "label": "(GMT-08:00) Pacific Time (US & Canada)", "value": "-8" },
    { "label": "(GMT-08:00) Tijuana, Baja California", "value": "-8" },
    { "label": "(GMT-07:00) Arizona", "value": "-7" },
    { "label": "(GMT-07:00) Chihuahua, La Paz, Mazatlan", "value": "-7" },
    { "label": "(GMT-07:00) Mountain Time (US & Canada)", "value": "-7" },
    { "label": "(GMT-06:00) Central America", "value": "-6" },
    { "label": "(GMT-06:00) Central Time (US & Canada)", "value": "-6" },
    { "label": "(GMT-05:00) Bogota, Lima, Quito, Rio Branco", "value": "-5" },
    { "label": "(GMT-05:00) Eastern Time (US & Canada)", "value": "-5" },
    { "label": "(GMT-05:00) Indiana (East)", "value": "-5" },
    { "label": "(GMT-04:00) Atlantic Time (Canada)", "value": "-4" },
    { "label": "(GMT-04:00) Caracas, La Paz", "value": "-4" },
    { "label": "(GMT-04:00) Manaus", "value": "-4" },
    { "label": "(GMT-04:00) Santiago", "value": "-4" },
    { "label": "(GMT-03:30) Newfoundland", "value": "-3.5" },
    { "label": "(GMT-03:00) Brasilia", "value": "-3" },
    { "label": "(GMT-03:00) Buenos Aires, Georgetown", "value": "-3" },
    { "label": "(GMT-03:00) Greenland", "value": "-3" },
    { "label": "(GMT-03:00) Montevideo", "value": "-3" },
    { "label": "(GMT-02:00) Mid-Atlantic", "value": "-2" },
    { "label": "(GMT-01:00) Cape Verde Is.", "value": "-1" },
    { "label": "(GMT-01:00) Azores", "value": "-1" },
    { "label": "(GMT+00:00) Casablanca, Monrovia, Reykjavik", "value": "0" },
    { "label": "(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London", "value": "0" },
    { "label": "(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna", "value": "1" },
    { "label": "(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague", "value": "1" },
    { "label": "(GMT+01:00) Brussels, Copenhagen, Madrid, Paris", "value": "1" },
    { "label": "(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb", "value": "1" },
    { "label": "(GMT+01:00) West Central Africa", "value": "1" },
    { "label": "(GMT+02:00) Amman", "value": "2" },
    { "label": "(GMT+02:00) Athens, Bucharest, Istanbul", "value": "2" },
    { "label": "(GMT+02:00) Beirut", "value": "2" },
    { "label": "(GMT+02:00) Cairo", "value": "2" },
    { "label": "(GMT+02:00) Harare, Pretoria", "value": "2" },
    { "label": "(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius", "value": "2" },
    { "label": "(GMT+02:00) Jerusalem", "value": "2" },
    { "label": "(GMT+02:00) Minsk", "value": "2" },
    { "label": "(GMT+02:00) Windhoek", "value": "2" },
    { "label": "(GMT+03:00) Kuwait, Riyadh, Baghdad", "value": "3" },
    { "label": "(GMT+03:00) Moscow, St. Petersburg, Volgograd", "value": "3" },
    { "label": "(GMT+03:00) Nairobi", "value": "3" },
    { "label": "(GMT+03:00) Tbilisi", "value": "3" },
    { "label": "(GMT+03:30) Tehran", "value": "3.5" },
    { "label": "(GMT+04:00) Abu Dhabi, Muscat", "value": "4" },
    { "label": "(GMT+04:00) Baku", "value": "4" },
    { "label": "(GMT+04:00) Yerevan", "value": "4" },
    { "label": "(GMT+04:30) Kabul", "value": "4.5" },
    { "label": "(GMT+05:00) Yekaterinburg", "value": "5" },
    { "label": "(GMT+05:00) Islamabad, Karachi, Tashkent", "value": "5" },
    { "label": "(GMT+05:30) Sri Jayawardenapura", "value": "5.5" },
    { "label": "(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi", "value": "5.5" },
    { "label": "(GMT+05:45) Kathmandu", "value": "5.75" },
    { "label": "(GMT+06:00) Almaty, Novosibirsk", "value": "6" }, { "label": "(GMT+06:00) Astana, Dhaka", "value": "6" },
    { "label": "(GMT+06:30) Yangon (Rangoon)", "value": "6.5" },
    { "label": "(GMT+07:00) Bangkok, Hanoi, Jakarta", "value": "7" },
    { "label": "(GMT+07:00) Krasnoyarsk", "value": "7" },
    { "label": "(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi", "value": "8" },
    { "label": "(GMT+08:00) Kuala Lumpur, Singapore", "value": "8" },
    { "label": "(GMT+08:00) Irkutsk, Ulaan Bataar", "value": "8" },
    { "label": "(GMT+08:00) Perth", "value": "8" },
    { "label": "(GMT+08:00) Taipei", "value": "8" },
    { "label": "(GMT+09:00) Osaka, Sapporo, Tokyo", "value": "9" },
    { "label": "(GMT+09:00) Seoul", "value": "9" },
    { "label": "(GMT+09:00) Yakutsk", "value": "9" },
    { "label": "(GMT+09:30) Adelaide", "value": "9.5" },
    { "label": "(GMT+09:30) Darwin", "value": "9.5" },
    { "label": "(GMT+10:00) Brisbane", "value": "10" },
    { "label": "(GMT+10:00) Canberra, Melbourne, Sydney", "value": "10" },
    { "label": "(GMT+10:00) Hobart", "value": "10" },
    { "label": "(GMT+10:00) Guam, Port Moresby", "value": "10" },
    { "label": "(GMT+10:00) Vladivostok", "value": "10" },
    { "label": "(GMT+11:00) Magadan, Solomon Is., New Caledonia", "value": "11" },
    { "label": "(GMT+12:00) Auckland, Wellington", "value": "12" },
    { "label": "(GMT+12:00) Fiji, Kamchatka, Marshall Is.", "value": "12" },
    { "label": "(GMT+13:00) Nuku'alofa", "value": "13" }
]

function timezoneSelect() {
    var options = [],
        select = document.createElement("select");
    select.setAttribute('class', 'time-location-select');
    // select.setAttribute('class', 'time-location-select');
    select.setAttribute('id', 'selectedclick');


    for (var i = 0; i < tzInts.length; i++) {
        var tz = tzInts[i],
            option = document.createElement("option");

        option.value = tz.value
        option.appendChild(document.createTextNode(tz.label))
        let selecttime = select.appendChild(option);
        if (document.querySelector('.time-stamp-list .select-wrapper')) {
            document.querySelector('.time-stamp-list .select-wrapper').appendChild(select)
        }
    }

    return select;
}

timezoneSelect();



$('select').on('select2:open', function (e) {
    if ($('.select2-container--open .select2-dropdown--below').hasClass('time-zoon-drop')) {
        $('.time-zoon-drop').attr('id', 'timezonedropdown');
        const simpleBar2 = new SimpleBar(document.getElementById('timezonedropdown').querySelector('.select2-results'));
        simpleBar2.getContentElement();
    }

});

//--- GMT Time JSON Object End ---//

//side menu script start
const side_menu_name = document.querySelectorAll('.side-layout .sidebar-nav>ul li');
for (const element of side_menu_name) {
    element.addEventListener("mouseenter", function () {
        element.parentElement.parentElement.parentElement.classList.add('expand');
        document.querySelector('.body-overlay').classList.add('selected');
    })
}

for (const element of side_menu_name) {
    element.addEventListener("mouseleave", function () {
        element.parentElement.parentElement.parentElement.classList.remove('expand');
        document.querySelector('.body-overlay').classList.remove('selected');
    })
}
//side menu script end

if (document.querySelector(".filter-popup-wrap .accordion")) {
    var acc = document.querySelectorAll(".filter-popup-wrap .accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
}

//tags close event script start
const tags = document.querySelectorAll('.tags-filter-wrap ul.tags-wrap > li a.close-icon');
for (const element of tags) {
    element.addEventListener('click', function () {
        element.parentElement.style.display = "none";
    })
}
//tags close event script end


//Time filter event script start
function time_input(x) {
    // const child_height = x.nextElementSibling.firstElementChild.clientHeight
    x.nextElementSibling.classList.add('tfilterexpand');
    document.querySelector('.body-overlay').classList.toggle('selected')
}
function timefilterclose(x) {
    x.parentElement.parentElement.parentElement.classList.remove('tfilterexpand');
    document.querySelector('.body-overlay').classList.remove('selected')
}


//Time filter event script start
function table_filter(x) {
    // const child_height = x.nextElementSibling.firstElementChild.clientHeight
    x.parentElement.classList.toggle('coloredth');
    x.nextElementSibling.classList.toggle('tfilterexpand');
    return false;
}

let tablefilter_pop = document.querySelectorAll('.common-popup .btn-applyfilter');
for (let tfp = 0; tfp < tablefilter_pop.length; tfp++) {
    tablefilter_pop[tfp].addEventListener('click', function (e) {
        e.target.parentElement.parentElement.previousElementSibling.insertAdjacentHTML('afterend', '<span class="icon ml-1"><img src="public/images/icon_open-pencil.svg" alt="" class="edit-icon" style="width:10px; height:10px"></span>')
        e.target.parentElement.parentElement.classList.remove('tfilterexpand');
        e.target.parentElement.parentElement.parentElement.classList.remove('coloredth');
    })
}


//-- radio toggle position styling using javascript --//
const toggleheight1 = document.querySelectorAll('.switch-horizontal label:first-of-type');
for (let element of toggleheight1) {
    const widths = element.clientWidth;
    element.parentElement.lastElementChild.style.left = (widths + 8) + 'px';
    element.parentElement.childNodes[1].style.left = (widths + 8) + 'px';
    element.parentElement.childNodes[5].style.left = (widths + 8) + 'px';
}

setTimeout(function () {
    if (document.querySelector(".dataTables_wrapper")) {
        let dw = document.querySelectorAll(".dataTables_wrapper");
        for (let goto = 0; goto < dw.length; goto++) {
            const child1 = document.createElement("input");
            child1.setAttribute('type', 'text');
            child1.setAttribute('placeholder', 'Page No');
            child1.setAttribute('value', '');
            const demo = document.createElement("button");
            demo.setAttribute('class', 'jumptopage');
            demo.textContent = 'Go';
            dw[goto].querySelector(".table-bi .goto").appendChild(child1);
            dw[goto].querySelector(".table-bi .goto").appendChild(demo);
        }

    }
}, 100);


//-- Multiselect checkbox script start--//
// document.onclick = function(e){
//     if(e.target.className !== 'btn-check-wrap'){
//         checkselect.classList.remove('arrow-toggle');
//         document.querySelector('.multiselect-checkbox .select-option-wrap').classList.remove('expanded-select');
//     }
//  };

// const checkselect = document.querySelectorAll('.multiselect-checkbox .btn-check-wrap');
// if(document.querySelector('.multiselect-checkbox .btn-check-wrap')) {
//     for(let cs=0; cs<checkselect.length; cs++) {
//         checkselect[cs].addEventListener('click', function(e){
//             e.preventDefault();
//             e.target.classList.toggle('arrow-toggle');
//             e.target.parentElement.querySelector('.multiselect-checkbox .select-option-wrap').classList.toggle('expanded-select');
//             return false;
//         })
//     }
// }
if (document.querySelector('.multiselect-checkbox .select-option-wrap')) {
    function multicheckdrop(e) {
        e.stopPropagation();
        e.preventDefault();
        e.target.classList.toggle('arrow-toggle');
        e.target.parentElement.querySelector('.multiselect-checkbox .select-option-wrap').classList.toggle('expanded-select');
        let alldatepicker = document.querySelectorAll('.dt-picker-wrap .datetimepicker');
        for (let adp = 0; adp < alldatepicker.length; adp++) {
            alldatepicker[adp].classList.remove('is-active')
        }
        return false;
    }
}



const checklistselect = document.querySelectorAll('.source-check-wrap .select-option-wrap .select-option-list li.single-check');
// let checkcount1 = 0;
for (let element of checklistselect) {

    let ulList = document.querySelector('.source-check-wrap .selected-items .tags-wrap');
    let sourcearray = [];
    element.firstElementChild.firstElementChild.addEventListener('change', function (e) {
        e.target.parentElement.parentElement.parentElement.querySelector('.checkbox-wrap').classList.add('check-minus');
        e.target.parentElement.parentElement.parentElement.querySelector('.checkbox-wrap').classList.remove('all-check');
        e.target.parentElement.parentElement.parentElement.querySelector('.checkbox-wrap').firstElementChild.checked = false;
        var checkboxes = document.querySelectorAll('.source-check-wrap .select-option-wrap .select-option-list li.single-check input[type="checkbox"]:checked');
        if (checkboxes.length === 1) {
            document.querySelector('.analysis-summary-wrap .summary-sec ul > li.total-row').classList.add('d-none');
            document.querySelector('.analysis-summary-wrap .summary-sec ul > li.no-data-found').classList.remove('d-none');
            document.querySelector('.analysis-summary-wrap .summary-sec .btn-download').parentElement.classList.add('d-none');
            document.querySelector('.entities-btn-process .btn-proceed').classList.add('disabled');
            let pagenav = document.querySelectorAll('.page-nav-steps .entities-steps > li a');
            for (let i = 0; i < pagenav.length; i++) {
                pagenav[1].classList.remove('highlight');
                pagenav[2].classList.remove('highlight')
                // pagenav[i].classList.remove('highlight');
            }
        } else {
            document.querySelector('.analysis-summary-wrap .summary-sec ul > li.total-row').classList.remove('d-none');
            document.querySelector('.analysis-summary-wrap .summary-sec ul > li.no-data-found').classList.add('d-none');
            document.querySelector('.analysis-summary-wrap .summary-sec .btn-download').parentElement.classList.remove('d-none');
            document.querySelector('.entities-btn-process .btn-proceed').classList.remove('disabled');
            let pagenav = document.querySelectorAll('.page-nav-steps .entities-steps > li a');
            for (let i = 0; i < pagenav.length; i++) {
                pagenav[1].classList.add('highlight');
                pagenav[2].classList.add('highlight')
            }
        }
        if (e.target.checked) {
            let targetVal = e.target.value;
            document.querySelector('.source-check-wrap .selected-items').parentElement.classList.remove('d-none');
            e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.firstElementChild.textContent = checkboxes.length;
            console.log(e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.querySelector('.text').innerText = "Selected")
            sourcearray.push(targetVal);
            let arrayLi = document.createElement('li');
            arrayLi.innerHTML = '<div class="wrap"><span class="label font-w-s">' + targetVal + '</span><a href="javascript:void(0)" class="close-icon ml-3" onclick="tagclosefun(event)"><img src="public/images/icon_blue-close.svg" alt=""></a></div>';
            ulList.appendChild(arrayLi);

        } else {
            e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.firstElementChild.textContent = checkboxes.length;
            let ulListli = document.querySelectorAll('.source-check-wrap .selected-items .tags-wrap li');
            for (let i = 0; i < ulListli.length; i++) {
                if (e.target.value === ulListli[i].querySelector('span.label').textContent) {
                    ulListli[i].remove();
                }
            }

        }

        if (checkboxes.length === 0) {
            e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.firstElementChild.textContent = '';
            e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.querySelector('.text').innerText = "Selecte Source";
            document.querySelector('.all-check-value .checkbox-wrap').classList.remove('check-minus')
        }
    })
}

function tagclosefun(e) {
    for (let element1 of checklistselect) {
        if (element1.firstElementChild.firstElementChild.value === e.target.parentElement.previousElementSibling.innerHTML) {
            element1.firstElementChild.firstElementChild.checked = false;
        }
    }
    var checkboxes = document.querySelectorAll('.source-check-wrap .select-option-wrap .select-option-list li.single-check input[type="checkbox"]:checked');
    e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.firstElementChild.firstElementChild.firstElementChild.textContent = checkboxes.length;
    console.log(e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.firstElementChild.firstElementChild)
    e.target.parentElement.parentElement.parentElement.remove();
    let allcloseli = document.querySelectorAll('.analysis-sec .selected-items .tags-wrap > li');
    if (allcloseli.length === 0) {
        document.querySelector('.selected-items').parentElement.classList.add('d-none');
    }
}

if (document.querySelector('.source-check-wrap .select-option-wrap .select-option-list li.all-check-value')) {
    let checkcount2 = 0;
    let ulList1 = document.querySelector('.source-check-wrap .selected-items .tags-wrap');
    let sourcearray1 = [];
    document.querySelector('.source-check-wrap .select-option-wrap .select-option-list li.all-check-value').addEventListener('change', function (e) {

        if (e.target.checked) {
            for (var ii = 0; ii < checklistselect.length; ii++) {
                checklistselect[ii].firstElementChild.firstElementChild.checked = true;
                e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.firstElementChild.textContent = checklistselect.length;
                // console.log(e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild);

                let allcheckValue = (checklistselect[ii].firstElementChild.firstElementChild).value;
                document.querySelector('.source-check-wrap .selected-items').parentElement.classList.remove('d-none');
                sourcearray1.push(allcheckValue);
                let arrayLi1 = document.createElement('li');
                arrayLi1.innerHTML = '<div class="wrap"><span class="label font-w-s">' + allcheckValue + '</span><a href="javascript:void(0)" class="close-icon ml-3" onclick="tagclosefun(event)"><img src="public/images/icon_blue-close.svg" alt=""></a></div>';
                ulList1.appendChild(arrayLi1);
            }

        } else {
            for (var ia = 0; ia < checklistselect.length; ia++) {
                checklistselect[ia].firstElementChild.firstElementChild.checked = false;
                e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.firstElementChild.textContent = "";
                e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.querySelector('.text').innerText = "Select Source"
                document.querySelector('.source-check-wrap .selected-items').parentElement.classList.add('d-none');
                let ulListli1 = document.querySelectorAll('.source-check-wrap .selected-items .tags-wrap li');
                for (let i = 0; i < ulListli1.length; i++) {
                    ulListli1[i].remove();
                }
            }
        }
    })
}




//-- Multiselect checkbox script end--//


let filterlabelcount = 2;
function appendfiltersec(e) {
    e.target.style.display = "none";
    e.target.nextElementSibling.style.display = "block";
    const filterappend = document.querySelector('.append-filter-wrap');
    if (e.target.previousElementSibling.previousElementSibling.previousElementSibling.querySelector('.select-filter-option').value == 'Resolved Date') {
        filterappend.insertAdjacentHTML("beforeend", '<div class="row mb-4 pt-3 align-items-center">' +
            '<div class="col-sm-3">' +
            '<p class="mb-0 font-w-s">' +
            '<i class="icon"><img src="public/images/icon-selectfilter.svg" alt=""></i>' +
            '<span class="text ml-3">Select Filter ' + filterlabelcount++ + '</span>' +
            '</p>' +
            '</div>' +
            '<div class="col-sm-8">' +
            '<form class="form-wrap row">' +
            '<div class="col-sm-3">' +
            '<select class="form-control select-filter-option select-drop-value">' +
            '<option>ColName1</option>' +
            '<option>Resolved Date</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-sm-3">' +
            '<select class="form-control select-drop-value">' +
            '<option>Contains</option>' +
            '<option>Does not Contain</option>' +
            '<option>Equals</option>' +
            '<option>Not equal to</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-sm-6 dt-picker-wrap dt-range-picker-wrap ">' +
            // '<input id="my_dtp" data-format="dd/MM/yyyy" value="09-Mar-2021 to 11-Mar-2021" class="my_dtp_c form-control" >'+
            // '<input type="text" name="daterange"  value="2022-01-01 to 2022-01-15" class="my_dtp_c form-control" />'+
            '<input  class="bulmaCalendarRange my_dtp_c form-control" type="date" >' +
            '</div>' +
            '<a href="javascript:void(0)" class="filter-add" onclick="appendfiltersec(event)">+</a>' +
            '<a href="javascript:void(0)" class="filter-delete" onclick="deleteappendfiltersec(event)"><img src="public/images/icon-delete.svg" alt=""></a>' +
            '</form>' +
            '</div>' +
            '</div>');
        $('.select-drop-value').select2({
            selectOnClose: true,
            minimumResultsForSearch: -1,
        });

        const start = new Date(Date.now() - 24 * 60 * 60 * 1000);
        const end = new Date(Date.now() + 24 * 60 * 60 * 1000);

        bulmaCalendar.attach('.bulmaCalendarRange', {
            isRange: true,
            startDate: start,
            endDate: end,
            dateFormat: 'dd-MMM-yyyy'
        });

        let allnavsteps = document.querySelectorAll('.page-nav-steps .entities-steps > li a');
        for (let ans = 0; ans < allnavsteps.length; ans++) {
            allnavsteps[1].classList.remove('disabled');
            allnavsteps[2].classList.remove('disabled');
            allnavsteps[1].classList.add('highlight');
            allnavsteps[2].classList.add('highlight');
        }
        document.querySelector('.entities-btn-process .btn-proceed').classList.remove('disabled');
    } else {

        filterappend.insertAdjacentHTML("beforeend", '<div class="row mb-4 pt-3 align-items-center">' +
            '<div class="col-sm-3">' +
            '<p class="mb-0 font-w-s">' +
            '<i class="icon"><img src="public/images/icon-selectfilter.svg" alt=""></i>' +
            '<span class="text ml-3">Select Filter ' + filterlabelcount++ + '</span>' +
            '</p>' +
            '</div>' +
            '<div class="col-sm-8">' +
            '<form class="form-wrap row">' +
            '<div class="col-sm-3">' +
            '<select class="form-control select-filter-option select-drop-value">' +
            '<option>ColName1</option>' +
            '<option>Resolved Date</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-sm-3">' +
            '<select class="form-control select-drop-value">' +
            '<option>Contains</option>' +
            '<option>Does not Contain</option>' +
            '<option>Equals</option>' +
            '<option>Not equal to</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-sm-6">' +
            '<div class="form-wrap multiselect-checkbox  multiselect-custom-check">' +
            '<button class="btn-check-wrap" onclick="multicheckdrop(event)">' +
            ' <span class="count"></span>' +
            '<span class="text">String1, String2, and 4 other....</span>' +
            '</button>' +
            '<div class="select-option-wrap" data-simplebar>' +
            '<ul class="select-option-list">' +
            '<li data-value="all-select">' +
            '<div class="checkbox-wrap all-check">' +
            '<input type="checkbox" id="selectall1" name="" value="">' +
            '<label for="selectall1"><span>Select All</span></label>' +
            '</div>' +
            '</li>' +
            '<li data-value="datasource1">' +
            '<div class="checkbox-wrap">' +
            '<input type="checkbox" id="datasource12" name="" value="DataSource 1">' +
            '<label for="datasource12"><span>String1</span></label>' +
            '</div>' +
            '</li>' +
            '<li data-value="datasource2">' +
            '<div class="checkbox-wrap">' +
            '<input type="checkbox" id="datasource22" name="" value="DataSource 2">' +
            '<label for="datasource22"><span>String2</span></label>' +
            '</div>' +
            '</li>' +
            '<li data-value="datasource3">' +
            '<div class="checkbox-wrap">' +
            '<input type="checkbox" id="datasource32" name="" value="DataSource 3">' +
            '<label for="datasource32"><span>Strin3</span></label>' +
            '</div>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<a href="javascript:void(0)" class="filter-add" onclick="appendfiltersec(event)">+</a>' +
            '<a href="javascript:void(0)" class="filter-delete" onclick="deleteappendfiltersec(event)"><img src="public/images/icon-delete.svg" alt=""></a>' +
            '</form>' +
            '</div>' +
            '</div>');
        $('.select-drop-value').select2({
            selectOnClose: true,
            minimumResultsForSearch: -1,
        });


    }

    return false;
}

function deleteappendfiltersec(e) {
    e.target.style.display = "none";
    e.target.parentElement.parentElement.parentElement.parentElement.remove();
}


if (document.querySelector('.policy-parameter-wrap')) {
    let allformfocus = document.querySelectorAll('.policy-parameter-wrap .issue-tabs-wrap .form-control');
    for (let aff = 0; aff < allformfocus.length; aff++) {
        allformfocus[aff].addEventListener('focus', function (e) {
            e.target.parentElement.parentElement.parentElement.parentElement.parentElement.querySelector('.attributes-cond-btn').classList.remove('d-none');
            let pagenav = document.querySelectorAll('.page-nav-steps .entities-steps > li a');
            for (let i = 0; i < pagenav.length; i++) {
                pagenav[i].classList.remove('highlight')
            }
            document.querySelector('.entities-btn-process .btn-proceed').classList.add('disabled');
        })
    }

    setTimeout(function () {
        let allformfocus1 = document.querySelectorAll('.policy-parameter-wrap .issue-tabs-wrap .select2-container');
        for (let aff1 = 0; aff1 < allformfocus1.length; aff1++) {
            allformfocus1[aff1].addEventListener('click', function (e) {
                e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.querySelector('.attributes-cond-btn').classList.remove('d-none')
                let pagenav = document.querySelectorAll('.page-nav-steps .entities-steps > li a');
                for (let i = 0; i < pagenav.length; i++) {
                    pagenav[i].classList.remove('highlight')
                }
                document.querySelector('.entities-btn-process .btn-proceed').classList.add('disabled');
            })
        }
    }, 100)

}

if (document.querySelector('.attributes-cond-btn .btn-filled')) {
    let allsavebtn = document.querySelectorAll('.attributes-cond-btn .btn-filled');
    for (let a = 0; a < allsavebtn.length; a++) {
        allsavebtn[a].addEventListener('click', function (e) {
            e.target.parentElement.parentElement.classList.add('d-none');
            let pagenav = document.querySelectorAll('.page-nav-steps .entities-steps > li a');
            for (let i = 0; i < pagenav.length; i++) {
                pagenav[1].classList.add('highlight')
                pagenav[2].classList.add('highlight')
            }
            document.querySelector('.entities-btn-process .btn-proceed').classList.remove('disabled');
        })
    }
}


// modal popup open on load extract isssue
$("#extract-issue-options-modal").modal('show');

//-- filter popup script start ---//
if (document.querySelector('.filter-popup-event')) {
    document.querySelector('.filter-popup-event').addEventListener('click', function () {
        document.querySelector('.filter-popup-wrap').classList.add('expandfilter');
        document.querySelector('.body-overlay').classList.add('selected');
    });
}
if (document.querySelector('.filter-popup-wrap .close-icon')) {
    document.querySelector('.filter-popup-wrap .close-icon').addEventListener('click', function () {
        document.querySelector('.filter-popup-wrap').classList.remove('expandfilter');
        document.querySelector('.body-overlay').classList.remove('selected');
    });
}
//-- filter popup script end --//


//--- NetworkGraph Zoom in zoom out script start ---//
// let Ncount = 9;
// if (document.querySelector('.graphzoomin')) {
//     document.querySelector('.graphzoomin').addEventListener('click', function () {
//         document.querySelector('#networkcontainer').style.transform = "scale(0." + Ncount++ + ")";

//     })
// }

// if (document.querySelector('.graphzoomin')) {
//     document.querySelector('.graphzoomout').addEventListener('click', function () {
//         document.querySelector('#networkcontainer').style.transform = "scale(0." + Ncount-- + ")";
//     })
// }

//-- NetworkGraph Zoom in zoom out script end ---//


//-- Estate health view changes on selection ---//
function estatecatselect(e) {
    const selectview = document.querySelectorAll('.network-container .select-view');
    for (let ii = 0; ii < selectview.length; ii++) {
        if (e.target.value === selectview[ii].id) {
            selectview[ii].style.display = 'block';
        } else {
            selectview[ii].style.display = 'none';
        }
    }
}
// if(document.querySelector('.estate-catogory-select')){
//     document.querySelector('.estate-catogory-select').addEventListener('change', function(e){
//         const selectview =  document.querySelectorAll('.network-container .select-view');
//         for(let ii = 0; ii<selectview.length; ii++) {
//            if(e.target.value === selectview[ii].id) {
//              selectview[ii].style.display = 'block';
//            }else {
//              selectview[ii].style.display = 'none';
//            }
//         }
//      })
// }

//-- Estate health  view changes on selection --//



function toggleItem(elem) {
    for (var i = 0; i < elem.length; i++) {
        elem[i].addEventListener("click", function (e) {
            var current = this;
            for (var i = 0; i < elem.length; i++) {
                if (current != elem[i]) {
                    elem[i].classList.remove('active');
                } 
                // else if (current.classList.contains('active') === true) {
                //     current.classList.remove('active');
                // } 
                else {
                    current.classList.add('active');
                }
            }
            e.preventDefault();
        });
    };
}
toggleItem(document.querySelectorAll('.present-past-event .event-output-val'));
toggleItem(document.querySelectorAll('.checked-right-col .checked-data-wrap ul > li'));
toggleItem(document.querySelectorAll('.view-tags-wrap .tag-grid-wrap ul > li'));
toggleItem(document.querySelectorAll('.incident-table-event-wrap > li.incident-view-changes > a'));




//   document.querySelector('.all-check').addEventListener('click', function(e){
//     e.target.classList.toggle('all-checked');
//     const allcheckbox = document.querySelectorAll('.dataTables_wrapper table.dataTable tr');
//     for (let check = 0; check<allcheckbox.length; check++) {
//         const typecheck = allcheckbox[check].querySelector('td .checkbox-wrap input');
//         if(typecheck.type=='checkbox') {
//             typecheck.checked=true;
//         }
//     }
//   })

// function tablebtntoggle(e) {
//     console.log(e.target.next)
// }



if (document.querySelector('.incident-view-changes .incident-card-view')) {
    document.querySelector('.incident-view-changes .incident-card-view').addEventListener('click', function () {
        document.querySelector('.datatable-wrapper .top-status').style.display = 'none';
        document.querySelector('.incident-table-wrapper').style.display = 'none';
        document.querySelector('.incident-card-wrapper').style.display = 'block';
        const tableviewwrap = document.querySelectorAll('.incident-table-event-wrap > li.table-view-list');
        for (let a = 0; a < tableviewwrap.length; a++) {
            tableviewwrap[a].style.display = 'none'
        }
        const cardviewwrap = document.querySelectorAll('.incident-table-event-wrap > li.card-view-list');
        for (let b = 0; b < cardviewwrap.length; b++) {
            cardviewwrap[b].style.display = 'block'
        }
    })
}

if (document.querySelector('.incident-view-changes .incident-table-view')) {
    document.querySelector('.incident-view-changes .incident-table-view').addEventListener('click', function () {
        document.querySelector('.datatable-wrapper .top-status').style.display = 'block';
        document.querySelector('.incident-table-wrapper').style.display = 'block';
        document.querySelector('.incident-card-wrapper').style.display = 'none';
        const tableviewwrap = document.querySelectorAll('.incident-table-event-wrap > li.table-view-list');
        for (let a1 = 0; a1 < tableviewwrap.length; a1++) {
            tableviewwrap[a1].style.display = 'block'
        }
        const cardviewwrap = document.querySelectorAll('.incident-table-event-wrap > li.card-view-list');
        for (let b1 = 0; b1 < cardviewwrap.length; b1++) {
            cardviewwrap[b1].style.display = 'none'
        }
    })
}

var url = window.location.href;
console.log(url);
console.log(~url.indexOf("#incidenttw"));
if (~url.indexOf("#incidenttw")) {
    document.querySelector('.incident-table-wrapper').style.display = 'none';
    document.querySelector('.incident-card-wrapper').style.display = 'block';
    document.querySelector('.incident-table-event-wrap > li > a.incident-table-view').classList.remove('active');
    document.querySelector('.incident-table-event-wrap > li > a.incident-card-view').classList.add('active');
} else {
    document.querySelector('.incident-table-wrapper').style.display = 'block';
    document.querySelector('.incident-card-wrapper').style.display = 'none';
    document.querySelector('.incident-table-event-wrap > li > a.incident-table-view').classList.add('active');
    document.querySelector('.incident-table-event-wrap > li > a.incident-card-view').classList.remove('active');
}


//-- Present past and active on click --//

//------------- Script for table grid View toggle option ---------------------------//
if (document.querySelector('.card-view-list .groupby-select')) {
    document.querySelector('.card-view-list .groupby-select').addEventListener('click', function (e) {
        e.target.classList.toggle('expand-option');
        e.target.nextElementSibling.classList.toggle('expand-dropdown');
    })
}
//------------- Script for table grid View toggle option ---------------------------//

//------------- Script for table grid View filter option ---------------------------//
const carddropdownlist = document.querySelectorAll('.card-view-list .card-multidropdown > ul.abc > li');
for (let e = 0; e < carddropdownlist.length; e++) {
    carddropdownlist[e].querySelector('a').addEventListener('click', function (event) {
        let textval = event.target.text;
        let aaa = document.querySelector('.card-view-list .groupby-select').textContent = textval;
        const carddropdownlist1 = document.querySelectorAll('.card-view-list .card-multidropdown > ul.abc > li > ul.card-subdropdown > li');
        for (let e1 = 0; e1 < carddropdownlist1.length; e1++) {
            carddropdownlist1[e1].querySelector('a').addEventListener('click', function (event1) {
                const textval1 = event1.target.text;
                const finalval = document.querySelector('.card-view-list .groupby-select').textContent = textval+ '-' +textval1;

                document.querySelector('.incident-table-event-wrap > li.card-view-list .card-multidropdown').classList.remove('expand-dropdown');
                document.querySelector('.incident-table-event-wrap > li.card-view-list .select-wrapper .groupby-select').classList.remove('expand-option');
                const allincidentcard = document.querySelectorAll('.incident-card-wrapper .incident-card-view-wrap');
                for (let cardelement of allincidentcard) {
                    console.log('dsd: ' + cardelement.dataset);
                    if (finalval === cardelement.getAttribute('data-a')) {
                        cardelement.style.display = 'block';
                    }else if (finalval === cardelement.getAttribute('data-b')) {
                        cardelement.style.display = 'block';
                    }else if (finalval === cardelement.getAttribute('data-c')) {
                        cardelement.style.display = 'block';
                    }else if (finalval === cardelement.getAttribute('data-d')) {
                        cardelement.style.display = 'block';
                    } else {
                        cardelement.style.display = 'none';
                    }
                    
                }
            })
        }


    })
}
//------------- Script for table grid View filter option ---------------------------//

setTimeout(function () {
    if (document.querySelector('.table-wrapper')) {
        document.querySelector('.table-wrapper').setAttribute('id', 'tablescrollbody');

        const simpleBar1 = new SimpleBar(document.getElementById('tablescrollbody'));
        simpleBar1.getContentElement();
    }
}, 1000)





function columnfilterfunc(event) {
    for (let checkElement of tablechecked) {
        let checkedinputval = checkElement.querySelector('input');
        if (checkedinputval.value === event.target.parentElement.getAttribute('data-val')) {
            checkedinputval.checked = false;
            event.target.parentElement.remove();
        }
    }
}

//--- Column sorting end ---//

let aaa = document.querySelectorAll('.trand-statics-wrap .toggle-wrapper input');
for (let element of aaa) {
    element.addEventListener('change', function (e) {
        let bbb = document.querySelectorAll('.trand-statics-wrap .data-wrapper');
        for (let element1 of bbb) {
            if (element1.getAttribute('data-val') === e.target.getAttribute('id')) {
                element1.style.display = 'block'
            } else { element1.style.display = 'none' }
        }
    })
}

let editableinput = document.querySelectorAll('.patterns-define-table table tr td .edit-icon');
for (let editElement of editableinput) {
    editElement.addEventListener('click', function (e) {
        this.previousElementSibling.focus();
        this.previousElementSibling.select();

    })
}

let editableinput1 = document.querySelectorAll('.patterns-define-table table tr td .edit-icon-drop');
for (let editElement of editableinput1) {
    editElement.addEventListener('click', function (e) {
        e.target.parentElement.parentElement.querySelector('.select2-container').classList.remove('select2-container--disabled')
        e.target.parentElement.parentElement.querySelector('.select2-container').classList.add('select2-container--active')
    })
}

$('.select-drop-value-1').on('select2:close', function (e) {
    $('.patterns-define-table table tr td .select2-container--default').removeClass('select2-container--active')
});

let closablerow = document.querySelectorAll('.patterns-define-table table tr td .close-icon');
for (let closeElement of closablerow) {
    closeElement.addEventListener('click', function (e) {
        this.parentElement.parentElement.remove();
    })
}




let radiocondition = document.querySelectorAll('.create-pattern-wrap .radio-wrap input');
for (let radioelement of radiocondition) {
    radioelement.addEventListener('change', function (event) {
        let patternbody = document.querySelectorAll('.create-pattern-wrap .radio-wrapper .pattern-body  > div');
        for (let patternelement of patternbody) {
            if (patternelement.getAttribute('data-val') === event.target.getAttribute('id')) {
                patternelement.style.display = "block";

            } else {
                patternelement.style.display = 'none'
            }
        }
    })
}


/*--- Convert Select Option to Ul Li using javascript start ---*/
if (document.querySelector('.custom-select-wrap ')) {
    document.querySelector('.custom-select-wrap ').innerHTML += '<div class="action-select-button"></div>';
    document.querySelector('.custom-select-wrap ').innerHTML += '<ul class="action-select-list"></ul>';

    let actionoption = document.querySelectorAll('.custom-select-wrap  select option');
    for (let optionele of actionoption) {
        document.querySelector('.table-view-list .action-select-list').innerHTML += '<li class="clsAnchor">' +
            '<span value="' + optionele.value + '" >' + optionele.text + '</span></li>';
    }

    let actionulli = document.querySelectorAll('.custom-select-wrap  ul.action-select-list li.clsAnchor');
    for (let ulelement of actionulli) {
        if (ulelement.querySelector('span').innerHTML == document.querySelector('.custom-select-wrap  select').selectedOptions[0].text) {
            ulelement.classList.add('selected');
        }
        if (ulelement.querySelector('span').getAttribute('value') == "secondoption") {
            let actionoption1 = document.querySelectorAll('.custom-select-wrap  select option');
            for (let bb = 0; bb < actionoption1.length; bb++) {
                let targtemodalval = actionoption1[1].getAttribute('data-target');
                ulelement.querySelector('span').setAttribute('data-toggle', "modal");
                ulelement.querySelector('span').setAttribute('data-target', targtemodalval);
            }

        }
    }
    setTimeout(() => {
        document.querySelector('.action-select-button').innerHTML = '<span>' + document.querySelector('.select-wrapper.custom-select-wrap .form-control').selectedOptions[0].text + '</span>' + '<a href="javascript:void(0);" class="select-list-link" onclick="toggleSelectOption()"><img src="public/images/icon-dropdown.svg" alt=""></a>';
    }, 1000);

    let selectlistspan = document.querySelectorAll('.custom-select-wrap  ul.action-select-list li.clsAnchor');
    for (let selectele of selectlistspan) {
        selectele.querySelector('span').addEventListener('click', function (e) {
            let selectlistspan1 = document.querySelectorAll('.custom-select-wrap  ul.action-select-list li.clsAnchor');
            for (let aa = 0; aa < selectlistspan1.length; aa++) {
                selectlistspan1[aa].classList.remove('selected')
            }
            let dd_val = this.getAttribute('value');
            document.querySelector('.action-select-button').innerHTML = '<span>' + this.innerText + '</span>' + '<a href="javascript:void(0);" class="select-list-link" onclick="toggleSelectOption()"><img src="public/images/icon-dropdown.svg" alt=""></a>'
            this.parentElement.classList.add('selected');
            e.target.parentElement.parentElement.classList.remove('open-select');
            document.querySelector('.custom-select-wrap .form-control[name=options]').value = dd_val;
            if (dd_val == "secondoption") {
                this.setAttribute('data-toggle', "modal");
            }
        })
    }

    function toggleSelectOption() {
        for (let toggleselect of selectlistspan) {
            toggleselect.parentElement.classList.toggle('open-select')
        }
    }
}


if (document.querySelector('.btn-extract-issue')) {
    document.querySelector('.btn-extract-issue').addEventListener('click', function () {
        setTimeout(function () {
            window.location = "https://think.design/workspace/tcs-digitate-code/issue-results.html";
        }, 3000)
    })
}

if (document.querySelector('.btn-extract-cls')) {
    document.querySelector('.btn-extract-cls').addEventListener('click', function () {
        setTimeout(function () {
            window.location = "https://think.design/workspace/tcs-digitate-code/cls-results.html";
        }, 3000)
    })
}

if (document.querySelector('.entities-btn-process .btn-proceed')) {
    document.querySelector('.entities-btn-process .btn-proceed').addEventListener('click', function (e) {
        this.nextElementSibling.classList.toggle('expand')
    })
}


if (document.getElementById("btnFileUpload")) {
    var fileupload = document.getElementById("files");
    var filePath = document.getElementById("selectedFiles");
    var button = document.getElementById("btnFileUpload");
    button.onclick = function () {
        fileupload.click();
    };
    fileupload.onchange = function () {
        var fileName = fileupload.value.split('\\')[fileupload.value.split('\\').length - 1];
        filePath.innerHTML = "<ul><li>" + fileName + "</li></ul>" + fileName;
    };

    var selDiv = "";
    var storeuploadfile = [];

    document.addEventListener("DOMContentLoaded", init, false);

    function init() {
        document.querySelector('#files').addEventListener('change', handleFileSelect, false);
        selDiv = document.querySelector("#selectedFiles");
    }

    function handleFileSelect(e) {

        if (!e.target.files || !window.FileReader) return;

        selDiv.innerHTML = "";

        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        filesArr.forEach(function (f) {
            if (!f.type.match("image.*")) {
                return;
            }

            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('myForm').classList.add('row');
                document.querySelector('.drag-and-drop-file .file-uploader-flow').classList.add('col-sm-9');
                document.getElementById('upload-list-wrap').style.display = "inline-block";
                document.getElementById('upload-list-wrap').classList.add('col-sm-3');
                // var html = "<li><img src=\"" + e.target.result + "\">" + f.name + "<a href='javascript:void(0)'>x</a></li>";
                var html = "<li><img src='public/images/icon-editor-insert_drive_file.svg'><span class='lbl'>" + f.name + "</span><a href='javascript:void(0)' onclick='fileuploadclose(event)'>&times;</a></li>";

                selDiv.innerHTML += html;
            }
            reader.readAsDataURL(f);

        });


    }
}

function fileuploadclose(e) {
    e.target.parentElement.remove();
    let uploadfilelength = document.querySelectorAll('.drag-and-drop-file #selectedFiles > li').length;
    if (uploadfilelength === 0) {
        document.querySelector('.drag-and-drop-file .file-uploader-flow').classList.remove('col-sm-9');
        document.getElementById('myForm').classList.remove('row');
        document.getElementById('upload-list-wrap').style.display = "none";
        document.getElementById('upload-list-wrap').classList.remove('col-sm-3');
    }
}




if (document.querySelector('.covered-tabs-nav .nav-tabs .nav-item .nav-link')) {
    let allnavlink = document.querySelectorAll('.covered-tabs-nav .nav-tabs .nav-item .nav-link');
    for (let anl = 0; anl < allnavlink.length; anl++) {
        allnavlink[1].addEventListener('click', function (e) {
            e.target.parentElement.previousElementSibling.previousElementSibling.firstElementChild.classList.add('activated');
            document.querySelector('.page-nav-steps .btn-wrap .btn-next').style.display = "none";
            document.querySelector('.page-nav-steps .btn-wrap .btn-border').style.display = "none";
            document.querySelector('.page-nav-steps .btn-wrap .btn-proceed').style.display = "inline-block";
            // document.querySelector('.page-nav-steps .btn-wrap .btn-proceed').setAttribute('onclick', "document.location.href='extract-cls.html';")
        })

        allnavlink[0].addEventListener('click', function (e) {
            e.target.parentElement.nextElementSibling.nextElementSibling.firstElementChild.classList.remove('activated');
            document.querySelector('.page-nav-steps .btn-wrap .btn-next').style.display = "inline-block";
            document.querySelector('.page-nav-steps .btn-wrap .btn-border').style.display = "inline-block";
            document.querySelector('.page-nav-steps .btn-wrap .btn-proceed').style.display = "none";
            // document.querySelector('.page-nav-steps .btn-wrap .btn-proceed').setAttribute('onclick', "document.location.href='extract-cls.html';")
        })
    }
}


if (document.querySelector('.progress-graph > li ') && document.querySelector('.tooltip-popup')) {
    let bartooltip = document.querySelectorAll('.progress-graph > li ');
    for (let bt = 0; bt < bartooltip.length; bt++) {
        bartooltip[bt].addEventListener('mouseover', function () {
            let fff = bartooltip[bt].querySelector('.tooltip-popup').clientWidth;
            let ggg = bartooltip[bt].querySelector('.tooltip-popup').clientHeight;
            bartooltip[bt].querySelector('.tooltip-popup').style.left = '-' + ((fff - bartooltip[bt].clientWidth) / 2) + 'px';
            bartooltip[bt].querySelector('.tooltip-popup').style.top = '-' + ((ggg + bartooltip[bt].clientHeight) - 8) + 'px';
        })

    }
}

function titledropdown(e) {
    e.target.classList.toggle('open');
    e.target.nextElementSibling.classList.toggle('open');
}

function editinputfield(e) {
    e.target.parentElement.previousElementSibling.focus();
    e.target.parentElement.previousElementSibling.select()
}

/*--- Add New Pattern Script start ---*/
let ruleCount = 0;
let ruleCount1 = 0;
let ruleCount2 = 0;
let ruleCount11 = 0;
let ruleCount12 = 0;
let ruleCount21 = 0;
function newgroupEvent(e) {
    e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.nextElementSibling.insertAdjacentHTML('beforeend', '<div class="row form-rows"><div class="col-12 new-hidden-group ">' +
        '<div class=" pattern-body  p-3 border mr-5 ">' +
        '<div class="rule-pattern-wrap" data-val="rulepattern">' +
        '<div class="rule-head">' +
        '<div class="row">' +
        '<div class="col-4">' +
        '<ul class="and-or-toggle switch-radio">' +
        '<li class="active radio-block">' +
        '<input type="radio" id="radio-one' + ruleCount++ + '" name="switch-one' + ruleCount1++ + '" value="and" checked/>' +
        '<label for="radio-one' + ruleCount2++ + '">AND</label>' +
        '</li>' +
        '<li class="radio-block">' +
        '<input type="radio" id="radio-two' + ruleCount11++ + '" name="switch-one' + ruleCount12++ + '" value="or" />' +
        '<label for="radio-two' + ruleCount21++ + '">OR</label>' +
        '</li>' +
        '</ul>' +
        '</div>' +
        '<div class="col-8">' +
        '<ul class="rules-toggle">' +
        '<li>' +
        ' <a href="javascript:void(0)" onclick="newruleEvent(event)">' +
        '<span class="icon">+</span>' +
        '<span class="text">New Rule</span>' +
        '</a>' +
        '</li>' +
        '<li>' +
        '<a href="javascript:void(0)" onclick="newgroupEvent(event)">' +
        '<span class="icon">+</span>' +
        '<span class="text">New Group</span>' +
        '</a>' +
        '</li>' +
        '<li>' +
        '<a href="javascript:void(0)" class="d-flex align-items-center close-icon" onclick="deleterules(event)">' +
        '<span class="icon"><img src="public/images/icon-delete.svg" alt=""></span>' +
        '<span class="text">Delete</span>' +
        '</a>' +
        ' </li>' +
        ' </ul>' +
        '</div>' +
        ' </div>' +
        '</div>' +
        '<div class="form-wrapper">' +
        ' <div class="row form-rows">' +
        '<div class="col-sm-3">' +
        ' <div class="form-wrap">' +
        ' <div class="select-wrapper">' +
        '<select class="form-control select-drop-value">' +
        '<option value="Contains">Contains</option>' +
        '<option value="Contains1">Contains</option>' +
        '<option value="Contains2">Contains</option>' +
        '<option value="Contains3">Contains</option>' +
        '</select>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-8">' +
        '<div class="form-wrap">' +
        '<input type="text" value="" placeholder="Pattern String.." class="form-control">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="row form-rows">' +
        '<div class="col-sm-3">' +
        '<div class="form-wrap">' +
        '<div class="select-wrapper">' +
        ' <select class="form-control select-drop-value">' +
        '<option value="Contains">Contains</option>' +
        '<option value="Contains1">Contains</option>' +
        '<option value="Contains2">Contains</option>' +
        '<option value="Contains3">Contains</option>' +
        '</select>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-8">' +
        '<div class="form-wrap">' +
        '<input type="text" value="" placeholder="Pattern String.." class="form-control">' +
        '</div>' +
        '</div>' +
        '</div>' +

        '</div>' +

        '</div>' +
        '<div class="regex-pattern-wrap" data-val="regexpattern" style="display:none">' +
        '<div class="form-wrap">' +
        '<label class="label-control">Pattern String</label>' +
        '<input type="text" value="" class="form-control">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div></div>');
    $('.select-filter-option, .select-drop-value').select2({
        selectOnClose: true,
        minimumResultsForSearch: -1,
        containerCssClass: "error",
        dropdownCssClass: "test"
    });
}

function newruleEvent(e) {
    e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.nextElementSibling.insertAdjacentHTML('beforeend', '<div class="row align-items-center form-rows">' +
        '<div class="col-sm-3">' +
        '<div class="form-wrap">' +
        '<div class="select-wrapper">' +
        '<select class="form-control select-drop-value">' +
        '<option value="Contains">Contains</option>' +
        '<option value="Contains1">Contains</option>' +
        '<option value="Contains2">Contains</option>' +
        '<option value="Contains3">Contains</option>' +
        '</select>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-8">' +
        '<div class="form-wrap">' +
        '<input type="text" value="" class="form-control" placeholder="Pattern String..">' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-1 pl-0">' +
        '<a href="javascript:void(0)" class="d-flex align-items-center close-icon" onclick="deletenewrules(event)"><img src="public/images/icon-delete.svg" alt=""></a>' +
        '</div>');
    $('.select-filter-option, .select-drop-value').select2({
        selectOnClose: true,
        minimumResultsForSearch: -1,
        containerCssClass: "error",
        dropdownCssClass: "test"
    });
}

function deleterules(e) {
    e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.remove()
}

function deletenewrules(e) {
    e.target.parentElement.parentElement.parentElement.remove()
}
/*--- Add New Pattern Script end ---*/

function droptoggle(e) {
    e.target.parentElement.nextElementSibling.classList.toggle('expand-toggle');
    e.target.parentElement.classList.toggle('open-list');
    e.stopPropagation();
}


if (document.querySelector('td .action-dropdown')) {
    let actiondropdown = document.querySelectorAll('td .action-dropdown');
    for (let add = 0; add < actiondropdown.length; add++) {
        let add_all = actiondropdown[add].querySelectorAll('li');
        for (let bs = 0; bs < add_all.length; bs++) {
            add_all[bs].addEventListener('click', function (e) {
                e.target.parentElement.previousElementSibling.classList.remove('open-list');
                e.target.parentElement.classList.remove('expand-toggle');
                if (e.target.innerText === "Accept") {
                    e.target.parentElement.previousElementSibling.querySelector('.lbl').textContent = e.target.innerText;
                    e.target.parentElement.previousElementSibling.classList.add('btn-accept');
                    e.target.parentElement.previousElementSibling.classList.remove('btn-reject');
                } else if (e.target.innerText === "Reject") {
                    e.target.parentElement.previousElementSibling.querySelector('.lbl').textContent = e.target.innerText;
                    e.target.parentElement.previousElementSibling.classList.add('btn-reject');
                    e.target.parentElement.previousElementSibling.classList.remove('btn-accept');
                }
            })
        }
    }
}


if (document.querySelector('.workspace').classList.contains("entities-pages")) {
    document.querySelector('body').classList.add('entities-body');
    let sideallmenu = document.querySelectorAll('.side-layout .sidebar-nav > ul > li a');
    for (let slm = 0; slm < sideallmenu.length; slm++) {
        if (sideallmenu[slm].querySelector('.label').innerText === 'Configure Patterns') {
            sideallmenu[slm].nextElementSibling.classList.add('show');
            sideallmenu[slm].classList.add('open-menu')
        }
    }
} else if (document.querySelector('.workspace').classList.contains("leading-pages")) {
    document.querySelector('body').classList.add('leading-body');
    let sideallmenu = document.querySelectorAll('.side-layout .sidebar-nav > ul > li a');
    for (let slm = 0; slm < sideallmenu.length; slm++) {
        if (sideallmenu[slm].querySelector('.label').innerText === 'Dashboards') {
            sideallmenu[slm].nextElementSibling.classList.add('show');
            sideallmenu[slm].classList.add('open-menu')
        }
    }
}

if (document.querySelector('thead th#check .all-check')) {
    let maincheck = document.querySelectorAll('thead th#check .all-check');
    for (let mc of maincheck) {
        mc.addEventListener('change', function (e) {
            e.target.parentElement.classList.remove('check-minus');
            e.target.parentElement.classList.add('all-check');
            if (e.target.checked) {
                let alltr = document.querySelectorAll('.common-table-wrapper .dataTables_wrapper table tbody tr');
                for (let at = 0; at < alltr.length; at++) {
                    alltr[at].firstElementChild.querySelector('input').checked = true;
                }
            } else {
                let alltr = document.querySelectorAll('.common-table-wrapper .dataTables_wrapper table tbody tr');
                for (let at = 0; at < alltr.length; at++) {
                    alltr[at].firstElementChild.querySelector('input').checked = false;
                }
            }

        })
    }
}

if (document.querySelector('.common-table-wrapper table tr td .checkbox-wrap')) {
    let allchecks1 = document.querySelectorAll('.common-table-wrapper table tr td .checkbox-wrap');
    for (let acs1 = 0; acs1 < allchecks1.length; acs1++) {
        allchecks1[acs1].addEventListener('click', function (e) {
            if (!e.target.checked) {
                e.target.parentElement.parentElement.parentElement.parentElement.previousElementSibling.querySelector('.all-checked').classList.add('check-minus');
                e.target.parentElement.parentElement.parentElement.parentElement.previousElementSibling.querySelector('.all-checked').classList.remove('all-check');
                e.target.parentElement.parentElement.parentElement.parentElement.previousElementSibling.querySelector('.all-checked').firstElementChild.checked = false;
            }
        })
    }
}

if (document.querySelector('.check-main-wrap li .checkbox-wrap.single-check-val')) {
    let selectf = document.querySelectorAll('.check-main-wrap li .checkbox-wrap.single-check-val');
    for (let sf = 0; sf < selectf.length; sf++) {
        selectf[sf].addEventListener('change', function (e) {

            if (e.target.checked) {
                e.target.parentElement.parentElement.parentElement.parentElement.firstElementChild.querySelector('.checkbox-wrap').classList.add('check-minus');
                e.target.parentElement.parentElement.parentElement.parentElement.firstElementChild.querySelector('.checkbox-wrap').classList.remove('all-check');
                e.target.parentElement.parentElement.parentElement.parentElement.firstElementChild.querySelector('.checkbox-wrap').firstElementChild.checked = false;
                e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.querySelector('.text').textContent = e.target.value;
            }

            var checkboxes1 = document.querySelectorAll('.check-main-wrap li .checkbox-wrap.single-check-val input[type="checkbox"]:checked');
            if (checkboxes1.length === 0) {
                // e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.firstElementChild.textContent = '';
                e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild.querySelector('.text').innerText = "Selecte Filter";
                document.querySelector('.check-main-wrap li:first-child .checkbox-wrap').classList.remove('check-minus')
            }
        })
    }
}



if (document.querySelector('.submittion-steps')) {
    document.querySelector('.submittion-steps').classList.add('onload-submittion-steps')
}

if (document.querySelector('.use-algo-accord .card .card-header .checkbox-wrap')) {
    document.querySelector('.use-algo-accord .card .card-header .checkbox-wrap input').addEventListener('change', function (e) {
        if (e.target.checked) {
            let allalgolist = document.querySelectorAll('.algorithm-list > li .checkbox-wrap input[type=checkbox]');
            for (let all = 0; all < allalgolist.length; all++) {
                allalgolist[all].checked = true;
            }
        } else {
            let allalgolist = document.querySelectorAll('.algorithm-list > li .checkbox-wrap input[type=checkbox]');
            for (let all = 0; all < allalgolist.length; all++) {
                allalgolist[all].checked = false;
            }
        }
    })
}

// if(document.querySelector('.toast-filled-btn')) {
//     document.querySelector('.toast-filled-btn').addEventListener('click', function(){
//         document.querySelector('.toast.eh-toast-wrap').style.display = "block"
//     })
// }

function opentoast() {
    document.querySelector('.toast.eh-toast-wrap').style.display = "block";
    setTimeout(function () {
        document.querySelector('.toast.eh-toast-wrap').style.display = "none";
    }, 20000);
}


if (document.querySelector('.checkbox-wrap.all-check')) {
    let allchecks = document.querySelectorAll('.checkbox-wrap.all-check');
    for (let acs = 0; acs < allchecks.length; acs++) {
        allchecks[acs].addEventListener('click', function (e) {
            e.target.parentElement.classList.remove('check-minus');
            e.target.parentElement.classList.add('all-check');
            if (e.target.checked) {
                let alltr = document.querySelectorAll('.check-main-wrap li .checkbox-wrap.single-check-val');
                for (let at = 0; at < alltr.length; at++) {
                    alltr[at].querySelector('input').checked = true;
                }
            } else {
                let alltr = document.querySelectorAll('.check-main-wrap li .checkbox-wrap.single-check-val');
                for (let at = 0; at < alltr.length; at++) {
                    alltr[at].querySelector('input').checked = false;

                }
            }
        })
    }
}






setTimeout(function () {
    if (document.querySelector('.submittion-steps li.step-2 .circle .filled-circle')) {
        document.querySelector('.submittion-steps li.step-2 .circle .filled-circle').style.display = "none";
        document.querySelector('.submittion-steps li.step-2 .circle  img').style.display = "inline-block";
        document.querySelector('.submittion-steps li.step-2 .date-titme-stamp').classList.add('d-block');
        document.querySelector('.submittion-steps li.step-2 .dash').classList.add('d-none');
        document.querySelector('.submittion-steps li.step-2 .date-titme-stamp').classList.remove('d-none');
        document.querySelector('.submittion-steps li.step-2 .dash').classList.remove('d-block')
    }

}, 10300)
setTimeout(function () {
    if (document.querySelector('.submittion-steps li.sstep-3 .circle .filled-circle')) {
        document.querySelector('.submittion-steps li.sstep-3 .circle .filled-circle').style.display = "none";
        document.querySelector('.submittion-steps li.sstep-3 .circle  img').style.display = "inline-block";
        document.querySelector('.submittion-steps li.sstep-3  .status-title').classList.add('d-none');
        document.querySelector('.submittion-steps li.sstep-3  .status-title').classList.remove('d-block');
        document.querySelector('.submittion-steps li.sstep-3  .final-status').classList.remove('d-none');
        document.querySelector('.submittion-steps li.sstep-3  .final-status').classList.add('d-block');
        document.querySelector('.submittion-steps li.sstep-3 .date-titme-stamp').classList.remove('d-none');
        document.querySelector('.submittion-steps li.sstep-3 .date-titme-stamp').classList.add('d-block');
        document.querySelector('.submittion-steps li.sstep-3 .dash').classList.remove('d-block');
        document.querySelector('.submittion-steps li.sstep-3 .dash').classList.add('d-none');
        console.log(document.querySelector('.submittion-steps li.sstep-3 img').getAttribute('src'))
        if (document.querySelector('.submittion-steps li.sstep-3 img').getAttribute('src') === 'public/images/icon-green-tick-ellipse.svg') {
            document.querySelector('.covered-event-wrap .covered-ul p span.status').innerText = 'Complete';
            document.querySelector('.page-nav-steps .entities-steps > li:last-child a').style.background = '#1C4C58';
            document.querySelector('.page-nav-steps .entities-steps > li:last-child a').style.color = '#ffffff';
            document.querySelector('.page-nav-steps .entities-steps > li:last-child a').classList.remove('highlight');
            document.querySelector('.page-nav-steps .entities-steps > li:last-child a').classList.add('activated');
        } else {
            document.querySelector('.covered-event-wrap .covered-ul p span.status').innerText = 'Failed';
            document.querySelector('.page-nav-steps .entities-steps > li:last-child a').style.background = '#FD7575';
            document.querySelector('.page-nav-steps .entities-steps > li:last-child a').style.color = '#ffffff';
            document.querySelector('.page-nav-steps .entities-steps > li:last-child a').classList.remove('highlight');
            document.querySelector('.page-nav-steps .entities-steps > li:last-child a').classList.add('activated');
            document.querySelector('.page-nav-steps .entities-steps > li:last-child a').innerText = 'Failed'
        }

        setTimeout(function () {
            if (document.querySelector('.submittion-steps li.sstep-3  .final-status').innerHTML === "Failed") {
                window.location = "https://think.design/workspace/tcs-digitate-code/finalized-failed.html";
            } else {
                window.location = "https://think.design/workspace/tcs-digitate-code/finalized-complete.html";
            }

        }, 500)

    }
}, 20300)


function openPopup(e) {
    if (e.target.value === "modify-issue") {
        $('#modify-issue-modal').modal('show')
    }
}
if (document.querySelector('.select-drop-value')) {
    setTimeout(function () {
        let allselectoptions = document.querySelectorAll('.select-drop-value option:first-child')

        for (let aso = 0; aso < allselectoptions.length; aso++) {
            if (allselectoptions[aso].getAttribute('data-val') === 'labled-option') {
                allselectoptions[aso].setAttribute("disabled", "disabled")
            }
        }
    }, 1000)
}



if (document.querySelector('.calendar-row.second-calender-row .checkbox-wrap')) {
    let secondcalcheck = document.querySelectorAll('.calendar-row.second-calender-row .checkbox-wrap');
    for (let scc = 0; scc < secondcalcheck.length; scc++) {
        secondcalcheck[scc].addEventListener('change', function (e) {
            if (e.target.checked) {
                e.target.parentElement.parentElement.classList.add('disabled')
            } else {
                e.target.parentElement.parentElement.classList.remove('disabled')
            }
            return false;
        })

        if (secondcalcheck[scc].querySelector('input').checked == true) {
            // alert('on load');
            secondcalcheck[scc].parentElement.classList.add('disabled')
        } else {
            secondcalcheck[scc].parentElement.classList.remove('disabled')
        }
    }
}

function trendsetting(e) {
    e.target.parentElement.nextElementSibling.classList.toggle('expand');
}




let v = document.querySelectorAll('.trand-statics-wrap .setting-dropdown li a');
for (let element of v) {
    element.addEventListener('click', function (e) {
        e.target.parentElement.parentElement.classList.remove('expand');
        if (e.target.innerHTML === "Timeline View") {
            e.target.parentElement.classList.add('d-none');
            e.target.parentElement.nextElementSibling.classList.remove('d-none');

        } else if (e.target.innerHTML === "Incident View") {
            e.target.parentElement.classList.add('d-none');
            e.target.parentElement.previousElementSibling.classList.remove('d-none');
        }

        let s = document.querySelectorAll('.chart-block');
        for (let element1 of s) {
            if (element1.getAttribute('data-id') === e.target.getAttribute('id')) {
                element1.classList.remove('d-none')
                // element1.style.display = 'block'
            } else {
                element1.classList.add('d-none')
                // element1.style.display = 'none'
            }
        }
    })
}



function trendselect(e) {
    console.log(e.target.value)

    let s = document.querySelectorAll('.chart-block');
    for (let element1 of s) {
        if (element1.getAttribute('data-id') === e.target.value) {
            element1.classList.remove('d-none')
            // element1.style.display = 'block'
        } else {
            element1.classList.add('d-none')
            // element1.style.display = 'none'
        }
    }
}





// // Prepare demo data
// // Data is joined to map using value of 'hc-key' property by default.
// // See API docs for 'joinBy' for more info on linking data and map.
// var data = [
//     ['in-ch', 19],
//         ['in-pb', 29],
//         ['in-rj', 30],
//         ['in-up', 31],
//         ['in-ut', 32],
//         ['in-jh', 33]
//     ];

//     // Create the chart
//     Highcharts.mapChart('map-container', {
//         chart: {
//             map: 'countries/in/in-all',
//             zoomType: 'x',
//             panning: true,
//             panKey: 'shift',
//             events: {
//               load: function() {
//                 this.mapZoom(0.39, 400, 7000, 0);
//               }
//             }
//         },

//         title: {
//             text: 'Highmaps basic demo'
//         },

//         subtitle: {
//             text: 'Source map: <a href="http://code.highcharts.com/mapdata/countries/in/in-all.js">India</a>'
//         },

//         mapNavigation: {
//             enabled: true,
//             buttonOptions: {
//                 verticalAlign: 'bottom'
//             }
//         },

//         colorAxis: {
//             min: 0
//         },

//         series: [{
//             data: data,
//             name: 'Random data',
//             states: {
//                 hover: {
//                     color: '#BADA55'
//                 }
//             },
//             dataLabels: {
//                 enabled: true,
//                 format: '{point.name}'
//             }
//         }]
//     });




let datetimetoggle = document.querySelectorAll('.column-dateandtime .left-nav ul.select-check-wrap > li .radio-wrap input');
for (let dtt = 0; dtt < datetimetoggle.length; dtt++) {
    datetimetoggle[dtt].addEventListener('change', function (e) {
        let dttwrap = document.querySelectorAll('.column-dateandtime .detail-wrapper .detail-block');
        for (let dttw = 0; dttw < dttwrap.length; dttw++) {
            if (e.target.getAttribute('id') === dttwrap[dttw].getAttribute('data-val')) {
                dttwrap[dttw].classList.remove('d-none');
            } else {
                dttwrap[dttw].classList.add('d-none');
            }
        }
    })
}


let colfiltercheck = document.querySelectorAll('.column-filter .select-check-wrap > li .checkbox-wrap');
for (let cfc = 0; cfc < colfiltercheck.length; cfc++) {
    colfiltercheck[cfc].querySelector('input').addEventListener('change', function (e) {
        if (e.target.checked) {
            e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.previousElementSibling.insertAdjacentHTML('afterend', '<span class="icon ml-1"><img src="public/images/icon_open-pencil.svg" alt="" class="edit-icon" style="width:10px; height:10px"></span>')
        }
    })
}












if (document.querySelector('.network-mao-tooltip-pop')) {
    function divMove(e) {
        const box = document.querySelector('#aaa');
        const rect = box.getBoundingClientRect();
        const heightval = document.querySelector('.network-mao-tooltip-pop').clientHeight;
        const widthval = document.querySelector('.network-mao-tooltip-pop').clientWidth;
        const finalleft = (rect.left + window.pageXOffset) - 110;
        console.log(widthval)
        const N_tooltip = document.querySelector('.network-mao-tooltip-pop');
        N_tooltip.style.top = ((rect.top + window.pageYOffset) + 27) + "px";
        N_tooltip.style.left = finalleft + "px";
        N_tooltip.style.display = "block";
    }

    function divMoveout(e) {
        const N_tooltip = document.querySelector('.network-mao-tooltip-pop');
        N_tooltip.style.display = "none";
    }
}





if (document.querySelector('.view-tags-wrap')) {
    let allgroupcheck = document.querySelectorAll('.view-tags-wrap .checkbox-wrap input');
    for (let agc = 0; agc < allgroupcheck.length; agc++) {
        allgroupcheck[agc].addEventListener('change', function (e) {
            let groupval = document.querySelectorAll('.tag-grid-wrap  .ul-group')
            if (e.target.checked && e.target.value === 'group-by') {
                document.querySelector('.tag-grid-wrap .group-by').classList.remove('d-none');
                document.querySelector('.tag-grid-wrap .non-group-by').classList.add('d-none');
            } else {
                document.querySelector('.tag-grid-wrap .non-group-by').classList.remove('d-none');
                document.querySelector('.tag-grid-wrap .group-by').classList.add('d-none');
            }
        })
    }
}





// Code By Webdevtrick ( https://webdevtrick.com )
var btn = document.querySelector('.add');
var remove = document.querySelector('.draggable');
var dragSrcEl;

function dragStart(e) {
//   this.style.opacity = '0.4';
  dragSrcEl = this;
  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/html', this.innerHTML);
};

function dragEnter(e) {
  this.classList.add('over');
}

function dragLeave(e) {
  e.stopPropagation();
  this.classList.remove('over');
}

function dragOver(e) {
  e.preventDefault();
  e.dataTransfer.dropEffect = 'move';
  return false;
}

function dragDrop(e) {
  if (dragSrcEl != this) {
    dragSrcEl.innerHTML = this.innerHTML;
    this.innerHTML = e.dataTransfer.getData('text/html');
    
  }
  return false;
}

function dragEnd(e) {
  var listItens = document.querySelectorAll('.draggable');
  [].forEach.call(listItens, function(item) {
    item.classList.remove('over');
  });
//   this.style.opacity = '1';
}

function addEventsDragAndDrop(el) {
  el.addEventListener('dragstart', dragStart, false);
  el.addEventListener('dragenter', dragEnter, false);
  el.addEventListener('dragover', dragOver, false);
  el.addEventListener('dragleave', dragLeave, false);
  el.addEventListener('drop', dragDrop, false);
  el.addEventListener('dragend', dragEnd, false);
}

var listItens = document.querySelectorAll('.draggable');
[].forEach.call(listItens, function(item) {
  addEventsDragAndDrop(item);
});

// function addNewItem() {
//   var newItem = document.querySelector('.input').value;
//   if (newItem != '') {
//     document.querySelector('.input').value = '';
//     var li = document.createElement('li');
//     var attr = document.createAttribute('draggable');
//     var ul = document.querySelector('ul');
//     li.className = 'draggable';
//     attr.value = 'true';
//     li.setAttributeNode(attr);
//     li.appendChild(document.createTextNode(newItem));
//     ul.appendChild(li);
//     addEventsDragAndDrop(li);
//   }
// }

// btn.addEventListener('click', addNewItem);


//--- Column sorting start ---//
let tablechecked = document.querySelectorAll('.table-column-filter-pop .check-list-wrap ul li .checkbox-wrap');
let checkedullist = document.querySelector('.table-column-filter-pop .checked-right-col .checked-data-wrap  ul');
for (let checkElement of tablechecked) {
    checkElement.querySelector('input').addEventListener('change', function (e) {
        if (e.target.checked) {
            let targetVal = e.target.value;
            let arrayLi = document.createElement('li');
            let dataval = arrayLi.setAttribute('data-val', targetVal);
            arrayLi.setAttribute('class','draggable');
            arrayLi.setAttribute('draggable', 'true');
            arrayLi.innerHTML = '<a href="javascript:void(0)" class="kabab-menu mr-2"><img src="public/images/icon-rearrange.svg" alt=""></a>' +
                '<span class="text">' + targetVal + '</span>' +
                '<a href="javascript:void(0)" class="close-data" onclick="columnfilterfunc(event)">&times;</a>';
            checkedullist.appendChild(arrayLi);
            addEventsDragAndDrop(arrayLi);
        } else {
            let alllistval = checkedullist.querySelectorAll('li');
            for (let dataval of alllistval) {
                if (e.target.checked === false && e.target.value === dataval.getAttribute('data-val')) {
                    dataval.remove();

                }
            }

        }
    })
}






















    var radius = 664 / 2, height = 267;

// var cluster = d3.layout.cluster()
//     .size([360, radius - 120]);

var cluster = d3.layout.tree().size([ radius * 2, height]);
// var tree = d3.layout.tree().size([width, height]);

var diagonal = d3.svg.diagonal.radial()
    .projection(function (d) {
    return [d.y, d.x / 180 * Math.PI];
});

var force = d3.layout.force()
    .linkDistance(80)
    .charge(-120)
    .gravity(.05)
    .size([ radius * 2, height]);



var svg = d3.select("#networkcontainer").append("svg").attr('id', "Ng")
    .attr("width", radius * 2)
    .attr("height", height).attr("viewBox", '0 0 1200 552').attr("xmlns", 'http://www.w3.org/2000/svg').attr("version", '1.1')
    .call(d3.behavior.zoom().on("zoom", redraw))
    .append("g")
    .attr('id', "g")
    .attr("transform", "translate(" + radius / 2 + ", 42)");

    function redraw() {
        svg.attr("transform",
            "translate(" + d3.event.translate + ")"
            + " scale(" + d3.event.scale + ")");
      }

var defs = svg.append("defs");
var marker = defs.append('marker')
    .attr('id', 'arrow')
    .attr("viewBox", "0 -5 10 10")
    .attr('markerWidth', 20)
    .attr('markerHeight', 18)
    .attr('refX', 29)
    // .attr('refY', '3')
    .attr('orient', 'auto')
    .attr('markerUnits', 'strokeWidth');
var mpath = marker.append('path').attr('d', "M0,-5L10,0L0,5").attr('fill', '#8189ad');


var filter = defs.append('filter')
    .attr('id', 'solid')
    .attr("x", "-0.1")
    .attr('y', "-0.1")
    .attr('width', '1.2')
    .attr('height', '1.2')
var feFlood = filter.append('feFlood').attr('flood-color', "#FCFCFC");
var feComposite = filter.append('feComposite').attr('in', "SourceGraphic").attr('operator', 'atop');

var data = {
    "name": "Timesheet",
    "fill": "#ffdd33",
    "stroke": "rgb(250 221 51 / 50%)",
    "img": "public/images/icon-network-apps.svg",
        "children": [{
        "name": "ts-appsvr-1",
        "fill": "#5cb85c",
        "stroke": "rgb(92 184 92 / 50%)",
        "img": "public/images/icon-network-database.svg",
            "children": [{
                "name": "rhel-appsvr-1",
                "img": "public/images/icon-network-appserver.svg",
                "fill": "#ffdd33",
                "stroke": "rgb(250 221 51 / 50%)",
                "children": [{
                        "name": "rhel-appsvr-1-1",
                        "fill": "#ffdd33",
                        "stroke": "rgb(250 221 51 / 50%)",
                        "img": "public/images/icon-network-apps.svg",
                        // "size": 3938
                    }, {
                        "name": "rhel-appsvr-1-2",
                        "fill": "#ffdd33",
                        "stroke": "rgb(250 221 51 / 50%)",
                        "img": "public/images/icon-network-apps.svg",
                        // "size": 3812
                    }, {
                        "name": "rhel-appsvr-1-3",
                        "fill": "#ffdd33",
                        "stroke": "rgb(250 221 51 / 50%)",
                        "img": "public/images/icon-network-apps.svg",
                        // "size": 6714
                    }
                ]
            },
            {
                "name": "ts-appsvr-1-1",
                "fill": "#5cb85c",
                "stroke": "rgb(92 184 92 / 50%)",
                "img": "public/images/icon-network-appserver.svg",
            }
        ]
    }, {
        "name": "ts-appsvr-2",
        "fill": "#e51722",
        "stroke": "rgb(229 23 34 / 50%)",
        "img": "public/images/icon-network-appserver.svg",
            "children": [{
            "name": "rhel-appsvr-2",
            "fill": "#ffdd33",
            "stroke": "rgb(250 221 51 / 50%)",
            "img": "public/images/icon-network-appserver.svg",
            // "size": 17010
        }]
    }]
};


var graph = {
    "links": [
        {
            "source": 2,
            "target": 1,
            "type": "Timesheet"
        },
        {
            "source": 3,
            "target": 1,
            "type": "ts-appsvr-1"
        },
        {
            "source": 5,
            "target": 2,
            "type": "ts-appsvr-1-1"
        },
        {
            "source": 6,
            "target": 2,
            "type": "ts-appsvr-1-1"
        },
        {
            "source": 7,
            "target": 5,
            "type": "ts-appsvr-1-1"
        },
        {
            "source": 8,
            "target": 5,
            "type": "ts-appsvr-1-1"
        },
        {
            "source": 9,
            "target": 5,
            "type": "ts-appsvr-1-1"
        },
        {
            "source": 3,
            "target": 4,
            "type": "ts-appsvr-1-1"
        },
        {
            "source": 4,
            "target": 2,
            "type": "ts-appsvr-1-1"
        },
        {
            "source": 10,
            "target": 3,
            "type": "ts-appsvr-1-1"
        },
       {
            "source": 11,
            "target": 4,
            "type": "uses"
        },
        {
            "source": 12,
            "target": 11,
            "type": "uses"
        },
       {
            "source": 13,
            "target": 12,
            "type": "uses"
        }
    ]
}

function update(root) {
    // var nodes = cluster.nodes(root);
    var nodes = cluster.nodes(data);
    var links = cluster.links(nodes);
  

    svg.selectAll("path.link").remove();
    
    svg.selectAll("g.node").remove();

    svg.selectAll("line").remove();

    var lines = svg.selectAll('line')
        .data(links)
        .enter()
        .append('line').attr("marker-end", "url(#arrow)")
        .attr('stroke','#000')
        .attr('x1',function(d){return d.source.x})
        .attr('y1',function(d){return d.source.y})
        .attr('x2',function(d){return d.target.x})
        .attr('y2',function(d){return d.target.y})
        // .attr("transform", function(d) {      
        //     return "rotate(" + (d.target.x - 90 ) + ")"; 
        // })

    // lines.attr("transform", function(d) {
    //  return "rotate(" + (d.source.x - 90 ) + ")translate(" + d.source.y + ")"; })



    
    // var link = svg.selectAll("path.link")
    //     .data(cluster.links(nodes))
    //     .enter().append("path")
    //     .attr("class", "link").attr("marker-end", "url(#arrow)")
    //     .attr("d", diagonal);
    // var Tooltip = d3.select("#ntooltip")
    // .style("opacity", 0)
    // .attr("class", "ntooltip")
    // .style("background-color", "white")
    // .style("border", "solid")
    // .style("border-width", "2px")
    // .style("border-radius", "5px")
    // .style("padding", "5px")
    // .style("position", "absolute")

    // var mouseover = function(d) {
    //     Tooltip
    //       .style("opacity", 1)
    //       Tooltip
    //       .html("<span style='color:grey'>Sepal length: </span>" + d.name) // + d.Prior_disorder + "<br>" + "HR: " +  d.HR)
    //       .style("left", (d3.mouse(this)[0]+130) + "px")
    //       .style("top", (d3.mouse(this)[1]+30) + "px")
    //     // d3.select(this)
    //     //   .style("stroke", "black")
    //     //   .style("opacity", 1)
    //   }
    //   var mousemove = function(d) {
    //     Tooltip
    //   .style("left", (d3.mouse(this)[0]+130) + "px")
    //   .style("top", (d3.mouse(this)[1]+30) + "px")
    //   }
    //   var mouseleave = function(d) {
    //     Tooltip
    //       .style("opacity", 0)
    //     // d3.select(this)
    //     //   .style("stroke", "none")
    //     //   .style("opacity", 0.8)
    //   }
    

    var node = svg.selectAll("g.node")
    .data(nodes)
        .enter().append("g")
        .attr("class", "node").attr("transform", function (d) { return "translate(" + d.x + "," + d.y + ")"; })
        .on("mouseover", mouseover)
        .on("mousemove", mousemove)
        .on("mouseleave", mouseleave)
        // .attr("transform", function (d) {
        //     return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")";
        // })

        



    node.append("circle")
        .attr("r", 32)
        .style("fill", function(d) { return d.fill; })
        .style('stroke-width', '19px').style('stroke', function (d) { return d.stroke })
        // .on("mouseover", mouseover)
        // .on("mousemove", mousemove)
        // .on("mouseleave", mouseleave)
        .on("click", click);
    
    node.append('image')
    .attr('href', function (d) { return d.img })
    .attr('transform', "translate(-14,-14)")
    .style('width', '28px').style('height', '28px')
    // .on("mouseover", mouseover)
    // .on("mousemove", mousemove)
    // .on("mouseleave", mouseleave)
    .on("click", click);

   

    node.append("text")
        .attr("dy", "3.2em")
        .attr("text-anchor", "middle").attr('fill', 'black')
        .style('font-size', '22px').attr('filter', "url(#solid)")
        .text(function (d) {
            return d.name;
        });
}
update(data);

var svgg = d3.select("#g");
// var svg = d3.select('#svg');

// var zoom = d3.behavior.zoom().scaleExtent([1, 8]).on("zoom", zoomed)
    
// svg.call(zoom);

// function zoomed() {
//   g.attr('transform', `translate(${d3.event.transform.x},  	 ${d3.event.transform.y}) scale(${d3.event.transform.k})`);
// };


// function transition(zoomLevel) {
//   svg.transition()
//       .delay(100)
//       .duration(700)
//       .call(zoom.scaleBy, zoomLevel);
//       //.call(zoom.transform, transform);
//       //.on("end", function() { canvas.call(transition); });
// }

// d3.selectAll('button').on('click', function() {
    
//   if (this.id === 'zoom_in') {
//     alert('increase')
//     transition(1.2); // increase on 0.2 each time
//   }
//   if (this.id === 'zoom_out') {
//     alert('decrease')
//     transition(0.8); // deacrease on 0.2 each time
//   }
//   if (this.id === 'zoom_init') {
//     alert('return')
//     svg.transition()
//         .delay(100)
//         .duration(700)
//         .call(zoom.scaleTo, 1); // return to initial state
//   }
// });

var zoomfactor = 1;

var zoomlistener = d3.behavior.zoom()
.on("zoom", redraw1);


d3.selectAll('#networkcontainer button').on('click', function() {
  if (this.id === 'zoom_in') {
    zoomfactor = zoomfactor + 0.2;
    zoomlistener.scale(zoomfactor).event(d3.select('#networkcontainer'));
  }
  if (this.id === 'zoom_out') {
    zoomfactor = zoomfactor - 0.2;
    zoomlistener.scale(zoomfactor).event(d3.select('#networkcontainer'));
  }
  if (this.id === 'zoom_init') {
    zoomfactor = 1;
    console.log('mini refresh', zoomfactor);
    zoomlistener.scale(zoomfactor).event(d3.select('#networkcontainer'));
  }
});




function redraw1() {
    console.log("here", d3.event.translate, d3.event.scale);
    svgg.attr("transform","translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")"); 
  } 


function collapse(d) {
    if (d.children) {
        d._children = d.children;
        d._children.forEach(collapse);
        d.children = null;
    }
}

data.children.forEach(collapse);
update(data);

// Toggle children on click.
function click(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update(data);
}

function mouseover() {
    let forcegraph = document.querySelectorAll(".network-graph-wrap #networkcontainer svg g.node");
    for(let fg = 0; fg<forcegraph.length; fg++) {
        forcegraph[fg].addEventListener('mouseenter', function(e) {
            e.target.classList.add('mimi')
            const gnode = e.target.getBoundingClientRect(); 
            const heightval = document.querySelector('#ntooltip').clientHeight;
            const widthval = document.querySelector('#ntooltip').clientWidth;
            const finalleft = (gnode.left + window.pageXOffset) - 47;
            const N_tooltip = document.querySelector('#ntooltip');
            N_tooltip.style.top = ((gnode.top + window.pageYOffset) + 32) + "px";
            N_tooltip.style.left = finalleft + "px";
            N_tooltip.style.display = "block";
            document.querySelector('#ntooltip .title').innerText = forcegraph[fg].querySelector('text').textContent;
        })

    }
}

function mousemove() {
    let forcegraph = document.querySelectorAll(".network-graph-wrap #networkcontainer svg g.node");
    for(let fg = 0; fg<forcegraph.length; fg++) {
        forcegraph[fg].addEventListener('mousemove', function(e) {
        const gnode = e.target.getBoundingClientRect();
        const heightval = document.querySelector('#ntooltip').clientHeight;
        const widthval = document.querySelector('#ntooltip').clientWidth;
        const finalleft = (gnode.left + window.pageXOffset) - 47;
        const N_tooltip = document.querySelector('#ntooltip');
        N_tooltip.style.top = ((gnode.top + window.pageYOffset) + 32) + "px";
        N_tooltip.style.left = finalleft + "px";
        N_tooltip.style.display = "block";
        document.querySelector('#ntooltip .title').innerText = forcegraph[fg].querySelector('text').textContent;
        })
    }
}

function mouseleave() {
    let forcegraph = document.querySelectorAll(".network-graph-wrap #networkcontainer svg g.node");
    for(let fg = 0; fg<forcegraph.length; fg++) {
        forcegraph[fg].addEventListener('mouseleave', function(e) {
            const N_tooltip = document.querySelector('#ntooltip');
            e.target.classList.remove('mimi')
            N_tooltip.style.display = "none";
        })
    }
}


      



if(document.querySelector('#upcoming-event-pie')){
    Highcharts.chart('upcoming-event-pie',{ 
        chart: {
            type: 'pie',
            height: (7.5 / 16 * 100) + '%', 
            style: {
                fontFamily: "Open Sans"
            }
        },
        title:{
            text: null
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<table  border="1" width="50" style="font-size:10px; ">',
            pointFormat: 
                    '<tr>'+
                        '<th style="color:{point.color}">\u25CF <b>{point.name}<b></th>'+
                        '<th></th>'+
                        '<th><b>{point.y}</b></th>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Suppressed</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:15px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>150</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Duplicate</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:17px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>175</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>False Positive</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:25px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>75</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Maintenance</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:10px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>100</td>'+
                    '</tr>',
            footerFormat: '</table>',
            shared: true,
            crosshairs: true,
            useHTML: true,
            borderWidth:0,
            backgroundColor: 'white'
                
        },
        plotOptions: {
            pie: {
                // size: 120,
                center: ["10%", "50%"],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            minPointSize: 10,
            innerSize: '60%',
            zMin: 0,
            name: 'countries',
            colorByPoint: true,
            data: [{
                name: 'Critical (300)',
                y: 200,
                z: 92.9,
            }, {
                name: 'Medium (350)',
                y: 300,
                z: 118.7
            }, {
                name: 'Low (350)',
                y: 500,
                z: 124.6
            }]
        }],
        colors: ['#E51722', '#F27B27', '#F8D422'],
        legend: {
            // layout: 'proximate',
            layout: 'vertical',
            align: 'right',
            floating:true,
            verticalAlign: 'middle',
            itemMarginTop: 5,
            itemMarginBottom: 5,
            symbolHeight: 7,
            symbolWidth: 7,
            symbolRadius: 7
        }
    });

    function redrawchart(){
        var chart = $('#upcoming-event-pie').highcharts();
        
        console.log('redraw');
        var w = $('#upcoming-event-pie').closest(".highcharts-figure-pie").width()
        // setsize will trigger the graph redraw 
        chart.setSize(       
            w,w * (2.1/4),false
        );
     }
     $(window).on('resize', redrawchart);
     redrawchart(); 
  
}



// $('#responsivepiecontainer').highcharts({
//     chart: {
//         plotBackgroundColor: null,
//         plotBorderWidth: null,
//         plotShadow: false
//    },
//     title: {
//         text: 'Responsive Resize'
//     },
//     tooltip: {
//         pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
//     },
//     plotOptions: {
//         pie: {
//             allowPointSelect: true,
//             cursor: 'pointer',
//             dataLabels: {
//                 enabled: true
//             }
//         }
//     },
//     series: [{
//         type: 'pie',
//         name: 'Browser share',
//         data: [
//             ['Firefox', 45.0],
//             ['IE',      26.8],
//             ['Safari',  8.5],
//             ['Opera',   6.2],
//             ['Others',  0.7]
//         ]
//     }]
// });

// function redrawchart(){
//     var chart = $('#responsivepiecontainer').highcharts();
    
//     console.log('redraw');
//     var w = $('#responsivepiecontainer').closest(".wrapper").width()
//     // setsize will trigger the graph redraw 
//     chart.setSize(       
//         w,w * (3/4),false
//     );
//  }

// $(window).resize(redrawchart);
// redrawchart();


// $('.network-graph-wrap #networkcontainer svg g.node').hover('hover', function(e){alert('hi')})

// $(".network-graph-wrap #networkcontainer svg g.node").on('hover',function(){

//     $('#ntooltip').css('opacity', '1')
// });

// setTimeout(function(){
//     $('.common-table-wrapper .dataTables_wrapper table > thead th .text').on('click', function(e){
//         e.stopPropagation();  
//         console.log('prevention')  
//       });
      
//       $('.common-table-wrapper .dataTables_wrapper table > thead th .hamburger').on('click', function(e){
//         e.stopPropagation();   
//         console.log('prevention')  
//       });

//       $('.common-popup').on('click', function(e){
//         e.stopPropagation(); 
//         console.log('prevention')    
//       });
// },100)

  