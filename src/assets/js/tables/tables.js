$(function(){
  let table = new DataTable('#table_id', {
    // options
    // dom: 'lrtp',
    // info: false,
    // "bLengthChange" : false,
    // pagingType: "full_numbers",
    // dom: 'Bfrtip',
    dom: "<'table-wrapper'tr>" +
    "<'row justify-content-end align-items-center''<'col-sm-4 table-bi'i><'col-sm-8 table-bi''<'d-flex justify-content-end align-items-center'lp'<'goto'>>>>",
    lengthMenu: [
      [ 5, 10, 25, 50, -1 ],
      [ '5 rows', '10 rows', '25 rows', '50 rows', 'Show all' ]
    ],
    colReorder: true,
    "oLanguage": {
        sLengthMenu: "_MENU_",
        "oPaginate": {
          "sNext": "",
          "sPrevious": "",
        }
    },
    columnDefs: [
      { "orderable": false, "targets": 'no-sort' }
    ],
    // "scrollX": true
    "preDrawCallback": function( settings ) {
        $(".select-drop-value").select2( { 
          tags: true,
          selectOnClose: true,
          minimumResultsForSearch: -1, 
        } );
    }
  });
  $('#search-inp').on('keyup',function(){
    table.search($(this).val()).draw() ;
  });

  let table1 = new DataTable('.table_id1', {
    // options
    // dom: 'lrtp',
    // info: false,
    // "bLengthChange" : false,
    // pagingType: "full_numbers",
    // dom: 'Bfrtip',
    dom: "<'table-wrapper'tr>" +
    "<'row justify-content-end align-items-center''<'col-sm-4 table-bi'i><'col-sm-8 table-bi''<'d-flex justify-content-end align-items-center'lp'<'goto'>>>>",
    lengthMenu: [
        [ 5, 10, 25, 50, -1 ],
        [ '5 rows', '10 rows', '25 rows', '50 rows', 'Show all' ]
    ],
    colReorder: true,
    "oLanguage": {
        sLengthMenu: "_MENU_",
        "oPaginate": {
          "sNext": "",
          "sPrevious": "",
        }
    },
    columnDefs: [
      { "orderable": false, "targets": 'no-sort' }
    ],
    // "scrollX": true
    "preDrawCallback": function( settings ) {
        $(".select-drop-value").select2( { 
          tags: true ,
          selectOnClose: true,
          minimumResultsForSearch: -1,
        } );
    }
  });
  $('#search-inp').on('keyup',function(){
      table1.search($(this).val()).draw() ;
  });


  // var table = $('#example').DataTable();
 
$('.common-table-wrapper .dataTables_wrapper table > thead th').on("click", function (event) {
    if($(event.target).is("span"))
        event.stopImmediatePropagation();
    else if($(event.target).is("a.hamburger"))
    event.stopImmediatePropagation();
});


})

