function splinecontainer() {
    //high chart
    var labels = ['0', '50', '100', '150', '200'];
    var data_notes_test = [{
        name: 'High',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x: 20.8,
        y: 0
    }, {
        name: 'Medium',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x: 31.1,
        y: 0
    },
    {
        name: 'Low',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x: 41.4,
        y: 0,
        
    }];

    

    if(document.querySelector('#splinecontainer')){
        let pastchart = Highcharts.chart('splinecontainer',{
            chart: {
                type: 'spline',
                height: '120px', // 16:9 ratio
                events: {
                    selection: function(event) {
                        updatePoint(this);
                    }
                },
                zoomType: 'xy',
                style: {
                    fontFamily: "Open Sans"
                }
            },
            title:{
                text: null
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['03/04/20 12:01:34 PM', '04/04/20 12:01:34 PM', '05/04/20 12:01:34 PM'],
                labels: {
                    enabled:false
                },
                plotBands: [{
                    color: '#FF000024',
                    from: 19.4,
                    to: 22.1
                },
                {
                    color: '#FF000024',
                    from: 30.4,
                    to: 31.8
                },
                {
                    color: '#FF000024',
                    from: 40.0,
                    to: 42.8
                }]
            },

            yAxis: {
                minorGridLineWidth: 1,
                gridLineWidth: 1,
                alternateGridColor: null,
                opposite: false,
                title: {
                    enabled: false
                },
                // labels: {
                //     formatter: function() {
                //         return labels[this.pos - 1]
                //     }
                // }
            },
            plotOptions: {
                spline: {
                    lineWidth: 2,
                    states: {
                        hover: {
                            lineWidth: 2
                        }
                    },
                    marker: {
                        enabled: false
                    }
                }
            },
            tooltip: {
                formatter: function() {
                  // this = point
                  return this.series.tooltipOptions.customTooltipPerSeries.call(this);
                },
                useHTML: true,
                // shared: true,
                // crosshairs: true,
                borderColor:"#dedede",
                backgroundColor: 'white',
                outside: true
              },
            
            series: [
                {
                    // name:'note',
                    type:'line',
                    data:data_notes_test,
                    zIndex: 2,
                    shared: false,
                    lineWidth: 0,
                    dataLabels: {
                        useHTML: true
                    },
                    tooltip: {
                        customTooltipPerSeries: function() {
                            return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em>'+
                            '<table style="width:100%">'+
                            '<tr>'+
                            '<td><span style="background:#E51722; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">High</span></td>'+
                            '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                            '</tr>'+
                            '<tr>'+
                            '<td><span style="background:#FF832B; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Medium</span></td>'+
                            '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                            '</tr>'+
                            '<tr>'+
                            '<td><span style="background:#FFDD33; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Low</span></td>'+
                            '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                            '</tr>'+
                        '</table>' 
                        }
                    }
                },
                {
                    name: 'High',
                    data: [ 5.7, 5.5, 5.9, 5.1, 5.5, 5.8, 6.0, 5.0, 8.1, 5.7, 5.5, 8.8,
                        8.9, 8.0, 8.8, 8.8, 8.0, 5.8, 5.0, 8.9, 9.2, 9.8, 9.5, 8.5,
                        9.5, 10.8, 18.0, 11.5, 12.0, 12.2, 12.5, 10.8, 9.9, 11.8, 11.5, 12.1,
                        11.8, 11.7, 12.5, 11.2, 10.8, 11.2, 12.1, 11.8, 12.0, 15.5, 15.5, 12.5,
                        11.1],
                        tooltip: {
                            customTooltipPerSeries: function() {
                                return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em>'+
                                '<table style="width:100%">'+
                                '<tr>'+
                                '<td><span style="background:#E51722; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">High</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                                '</tr>'+
                                '<tr>'+
                                '<td><span style="background:#FF832B; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Medium</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                                '</tr>'+
                                '<tr>'+
                                '<td><span style="background:#FFDD33; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Low</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                                '</tr>'+
                            '</table>' 
                            }
                        }
                },
                {
                    name: 'Medium',
                    data: [6.2, 6.1, 6.1, 6.1, 6.3, 6.2, 6.3, 6.1, 6.7, 6.3, 6.2, 6.2,
                        6.3, 6.1, 6.3, 6.8, 6.3, 6.2, 6.3, 6.2, 6.4, 6.6, 6.9, 6.3,
                        6.7, 1.1, 1.8, 1.2, 1.4, 1.2, 6.9, 6.8, 6.9, 6.2, 6.4, 1.2,
                        6.3, 2.3, 1.6, 6.7, 1.6, 6.8, 2.6, 1.2, 1.4, 3.7, 2.1, 2.6,
                        1.5],
                        tooltip: {
                            customTooltipPerSeries: function() {
                                return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em><table style="width:100%">'+
                                '<tr>'+
                                '<td><span style="background:#E51722; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">High</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                                '</tr>'+
                                '<tr>'+
                                '<td><span style="background:#FF832B; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Medium</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                                '</tr>'+
                                '<tr>'+
                                '<td><span style="background:#FFDD33; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Low</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                                '</tr>'+
                            '</table>' 
                            }
                        }
                },
                {
                    name: 'Low',
                    data: [4.7, 4.4, 4.9, 5.1, 4.5, 4.8, 4.0, 7.0, 6.1, 4.7, 4.4, 6.4,
                        6.9, 6.0, 6.8, 4.4, 4.0, 3.8, 7.0, 4.9, 9.2, 9.6, 9.7, 6.3,
                        9.7, 10.8, 14.0, 11.7, 10.0, 10.2, 10.3, 9.4, 8.9, 10.6, 10.7, 11.1,
                        10.4, 10.7, 11.3, 10.2, 9.6, 10.2, 11.1, 10.8, 13.0, 12.7, 12.7, 11.3,
                        10.1],
                        tooltip: {
                            customTooltipPerSeries: function() {
                                return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em><table style="width:100%">'+
                                '<tr>'+
                                '<td><span style="background:#E51722; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">High</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                                '</tr>'+
                                '<tr>'+
                                '<td><span style="background:#FF832B; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Medium</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                                '</tr>'+
                                '<tr>'+
                                '<td><span style="background:#FFDD33; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Low</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                                '</tr>'+
                            '</table>' 
                            }
                        }
                }
            ],
            colors: ['#E51722', '#FF832B', '#FFDD33'],

            navigation: {
                menuItemStyle: {
                    fontSize: '10px'
                }
            },
            legend: {
                enabled: false
            }
        })


        function redrawcharts(){
            var chart = $('#splinecontainer').highcharts();
            
            console.log('redraw');
                var w = $('#splinecontainer').closest(".highcharts-figure").width()
                var w1 = '120'
                chart.setSize(       
                    w,w1 ,false
                );
          
         }
        //  $(window).on('resize', redrawchart4);

        $('#past-col').on('click', function(){
           setTimeout(function(){
            redrawcharts();
           }, 100)
        })
    }
}

var updatePoint = function(point) {

    document.querySelector('.past-col').classList.add('col-12');
    document.querySelector('.past-col').classList.remove('col-16');
    document.querySelector('.present-col').classList.add('d-none');

    document.querySelector('#splinecontainer').parentElement.previousElementSibling.querySelector('p').innerHTML = "<strong>CUSTOM :</strong> <span>05/04/20 12:24:34 PM</span> <span style='font-size:14px; margin:0 4px'>&#x2192;</span> <span>05/04/20 12:24:34 PM</span>"
    splinecontainer();
};

splinecontainer();

let chartredraw = document.querySelectorAll('.tags-filter-wrap .days-wrap ul > li.present-past-event .event-output-val');
// for(let r = 0; r<chartredraw.length; r++) {
//     chartredraw[r].addEventListener('click', function(){

//         document.querySelector('.present-col ').style.display = 'none';

//         document.querySelector('.past-col ').style.display = 'block';
//         splinecontainer();
//     })
// }


function presentsplineconatainer() {
    var labels1 = ['0', '50', '100', '150', '200'];
    var data_notes_test1 = [{
        name: 'Test 1',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x: 20.8,
        y: 0
    }, {
        name: 'Test 2',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x: 31.1,
        y: 0
    },
    {
        name: 'Test 3',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x: 41.4,
        y: 0,
        
    }];
    
    if(document.querySelector('#container1')){
        let presentchart = Highcharts.chart('container1',{
            chart: {
                type: 'spline',
                height: '120px', // 16:9 ratio
                events: {
                    selection: function(event) {
                        updatePoint1(this);
                    }
                },
                zoomType: 'xy',
                style: {
                    fontFamily: "Open Sans"
                }
            },
            title:{
                text: null
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['03/04/20 12:01:34 PM', '04/04/20 12:01:34 PM', '05/04/20 12:01:34 PM'],
                labels: {
                    enabled:false
                },
                plotBands: [{
                    color: '#FF000024',
                    from: 19.4,
                    to: 22.1
                },
                {
                    color: '#FF000024',
                    from: 30.4,
                    to: 31.8
                },
                {
                    color: '#FF000024',
                    from: 40.0,
                    to: 42.8
                }]
            },
    
            yAxis: {
                minorGridLineWidth: 1,
                gridLineWidth: 1,
                alternateGridColor: null,
                opposite: false,
                title: {
                    enabled: false
                },
                // labels: {
                // formatter: function() {
                //     return labels1[this.pos - 1]
                // }
                // }
            },
            plotOptions: {
                spline: {
                    lineWidth: 2,
                    states: {
                        hover: {
                            lineWidth: 2
                        }
                    },
                    marker: {
                        enabled: false
                    }
                }
            },
            tooltip: {
                formatter: function() {
                  // this = point
                  return this.series.tooltipOptions.customTooltipPerSeries.call(this);
                },
                useHTML: true,
                // shared: true,
                // crosshairs: true,
                borderColor:"#dedede",
                backgroundColor: 'white',
                outside: true
              },
            
            series: [
                {
                    // name:'note',
                    type:'line',
                    data:data_notes_test1,
                    zIndex: 2,
                    shared: false,
                    lineWidth: 0,
                    tooltip: {
                        customTooltipPerSeries: function() {
                            return '<em style="font-weight:600; font-size:12px; color:#212121; font-style: normal;"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em><br/>'+
                            '<em style="font-weight:600; font-style: normal; font-size:11px; color:#212121; background:#FF000024; text-transform: uppercase; padding:1px 3px; margin:2px 0; display:inline-block">'+
                            '<img src="../public/images/icon-warning-charts.svg" alt="" height="11" width="11"> <span class="title">Threshold Crossed</span></em><br/>'+
                            '<em style="font-size:11px; font-style: normal; color:#212121; margin-bottom:3px; display="inline-block"><span style="font-weight:600;">Rule 1344 :</span> <span class="title">Total Events > 200</span></em><br/>'+
                            '<table style="width:100%">'+
                            '<tr>'+
                            '<td><span style="background:#E51722; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">High</span></td>'+
                            '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                            '</tr>'+
                            '<tr>'+
                            '<td><span style="background:#FF832B; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Medium</span></td>'+
                            '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                            '</tr>'+
                            '<tr>'+
                            '<td><span style="background:#FFDD33; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Low</span></td>'+
                            '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                            '</tr>'+
                        '</table>' 
                        }
                    }
                },
                {
                    name: 'High',
                    data: [ 5.7, 5.5, 5.9, 5.1, 5.5, 5.8, 6.0, 5.0, 8.1, 5.7, 5.5, 8.8,
                        8.9, 8.0, 8.8, 8.8, 8.0, 5.8, 5.0, 8.9, 9.2, 9.8, 9.5, 8.5,
                        9.5, 10.8, 18.0, 11.5, 12.0, 12.2, 12.5, 10.8, 9.9, 11.8, 11.5, 12.1,
                        11.8, 11.7, 12.5, 11.2, 10.8, 11.2, 12.1, 11.8, 12.0, 15.5, 15.5, 12.5,
                        11.1],
                    tooltip: {
                        customTooltipPerSeries: function() {
                            return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em>'+
                            '<table style="width:100%">'+
                            '<tr>'+
                            '<td><span style="background:#E51722; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">High</span></td>'+
                            '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                            '</tr>'+
                            '<tr>'+
                            '<td><span style="background:#FF832B; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Medium</span></td>'+
                            '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                            '</tr>'+
                            '<tr>'+
                            '<td><span style="background:#FFDD33; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Low</span></td>'+
                            '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                            '</tr>'+
                        '</table>' 
                        }
                    }
                },
                {
                    name: 'Medium',
                    data: [6.2, 6.1, 6.1, 6.1, 6.3, 6.2, 6.3, 6.1, 6.7, 6.3, 6.2, 6.2,
                        6.3, 6.1, 6.3, 6.8, 6.3, 6.2, 6.3, 6.2, 6.4, 6.6, 6.9, 6.3,
                        6.7, 1.1, 1.8, 1.2, 1.4, 1.2, 6.9, 6.8, 6.9, 6.2, 6.4, 1.2,
                        6.3, 2.3, 1.6, 6.7, 1.6, 6.8, 2.6, 1.2, 1.4, 3.7, 2.1, 2.6,
                        1.5],
                        tooltip: {
                            customTooltipPerSeries: function() {
                                return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em>'+
                                '<table style="width:100%">'+
                                '<tr>'+
                                '<td><span style="background:#E51722; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">High</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                                '</tr>'+
                                '<tr>'+
                                '<td><span style="background:#FF832B; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Medium</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                                '</tr>'+
                                '<tr>'+
                                '<td><span style="background:#FFDD33; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Low</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                                '</tr>'+
                            '</table>' 
                            }
                        }
                },
                {
                    name: 'Low',
                    data: [4.7, 4.4, 4.9, 5.1, 4.5, 4.8, 4.0, 7.0, 6.1, 4.7, 4.4, 6.4,
                        6.9, 6.0, 6.8, 4.4, 4.0, 3.8, 7.0, 4.9, 9.2, 9.6, 9.7, 6.3,
                        9.7, 10.8, 14.0, 11.7, 10.0, 10.2, 10.3, 9.4, 8.9, 10.6, 10.7, 11.1,
                        10.4, 10.7, 11.3, 10.2, 9.6, 10.2, 11.1, 10.8, 13.0, 12.7, 12.7, 11.3,
                        10.1],
                        tooltip: {
                            customTooltipPerSeries: function() {
                                return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em>'+
                                '<table style="width:100%">'+
                                '<tr>'+
                                '<td><span style="background:#E51722; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">High</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                                '</tr>'+
                                '<tr>'+
                                '<td><span style="background:#FF832B; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Medium</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                                '</tr>'+
                                '<tr>'+
                                '<td><span style="background:#FFDD33; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Low</span></td>'+
                                '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                                '</tr>'+
                            '</table>' 
                            }
                        }
                }
            ],
            colors: ['#E51722', '#FF832B', '#FFDD33'],
    
            // tooltip: {
            //     valueSuffix: ' m/s',
            //     borderWidth:0,
            //     backgroundColor: 'white'
            // },
            navigation: {
                menuItemStyle: {
                    fontSize: '10px'
                }
            },
            legend: {
                enabled: false
            }
        })

        function redrawchartsp(){
            var chart = $('#container1').highcharts();
            
            console.log('redraw');
                var w = $('#container1').closest(".highcharts-figure").width()
                var w1 = '120'
                chart.setSize(       
                    w,w1 ,false
                );
          
         }
        //  $(window).on('resize', redrawchartsp);

        $('#present-col').on('click', function(){
           setTimeout(function(){
            redrawchartsp();
           }, 100)
        })
    }
}

var updatePoint1 = function(point) {
    document.querySelector('.present-col').classList.add('col-12');
    document.querySelector('.present-col').classList.remove('col-16');
    document.querySelector('.past-col').classList.add('d-none');

    document.querySelector('#container1').parentElement.previousElementSibling.querySelector('p').innerHTML = "<strong>CUSTOM :</strong> <span>05/04/20 12:24:34 PM</span> <span style='font-size:14px; margin:0 4px'>&#x2192;</span> <span>05/04/20 12:24:34 PM</span>"
    presentsplineconatainer();
};

presentsplineconatainer()

for(let r = 0; r<chartredraw.length; r++) {
    chartredraw[r].addEventListener('click', function(e){
        let allstatus = document.querySelectorAll('.top-status');
        for(let as = 0; as<allstatus.length; as++) {
            if(chartredraw[r].getAttribute('id') === 'past-col') {
                allstatus[as].innerHTML = 'Past'
            }else if(chartredraw[r].getAttribute('id') === 'present-col') {
                allstatus[as].innerHTML = 'Present'
            }
    
        }
       
        let datavalue = document.querySelectorAll('.trend-timeline-view-wrap .trand-chart-container'); 
        for(let d = 0; d<datavalue.length; d++) {
            // datavalue[d].style.display = 'none'; 
            datavalue[d].classList.add('d-none'); 
            datavalue[d].classList.add('col-6');
            datavalue[d].classList.remove('col-12'); 
            // console.log(e.target.parentElement.getAttribute('id'))
            if(e.target.parentElement.getAttribute('id') === datavalue[d].getAttribute('data-val')){
                // datavalue[d].style.display = 'block';
                datavalue[d].classList.remove('d-none'); 
                datavalue[d].classList.add('col-12'); 
                datavalue[d].classList.remove('col-6');
                splinecontainer();
            }else {
                presentsplineconatainer()
            }
        }
        
    })
}

function chartrefresh(e) {
    let datavalue = document.querySelectorAll('.trend-timeline-view-wrap .trand-chart-container'); 
    for(let d = 0; d<datavalue.length; d++) {
        // datavalue[d].style.display = 'block';
        datavalue[d].classList.remove('d-none'); 
        datavalue[d].classList.remove('col-12'); 
        datavalue[d].classList.add('col-6');
        document.querySelector('#splinecontainer').parentElement.previousElementSibling.querySelector('p').innerHTML = "<strong>PAST</strong> 2D";
        document.querySelector('#container1').parentElement.previousElementSibling.querySelector('p').innerHTML = "<strong>Present</strong> 2H";
        splinecontainer();
        presentsplineconatainer()
    }
}

if(document.querySelector('#container4')){
    Highcharts.chart('container4',{
        chart: {
            type: 'pie',
            height: (6.5/ 16 * 100) + '%', // 16:9 ratio
            style: {
                fontFamily: "Open Sans"
            }
        },
        title:{
            text: null
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<table  border="1" width="100" style="font-size:11px; ">',
            pointFormat: 
                    '<tr>'+
                        '<th><span style="color:{point.color}">\u25CF</span> <b style="font-weight: 600">{point.name}<b></th>'+
                        '<th></th>'+
                        '<th><b style="font-weight: 600">{point.y}</b></th>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Suppressed</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:15px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>150</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Duplicate</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:17px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>175</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>False Positive</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:25px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>75</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Maintenance</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:10px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>100</td>'+
                    '</tr>',
            footerFormat: '</table>',
            shared: true,
            crosshairs: true,
            useHTML: true,
            borderWidth:0,
            outside: true,
            backgroundColor: 'white'
                
        },
        plotOptions: {
            pie: {
               // size: 120,
               center: ["10%", "50%"],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            minPointSize: 10,
            innerSize: '60%',
            zMin: 0,
            name: 'countries',
            colorByPoint: true,
            data: [{
                name: 'Actionable (200)',
                y: 200,
                z: 92.9,
            }, {
                name: 'Aggregated (300)',
                y: 300,
                z: 118.7
            }, {
                name: 'Suppressed (500)',
                y: 500,
                z: 124.6
            }]
        }],
        colors: ['#1D6885', '#45B4DF', '#A9DAEE'],
        legend: {
            // layout: 'proximate',
            layout: 'vertical',
            align: 'right',
            floating:true,
            verticalAlign: 'middle',
            itemMarginTop: 5,
            itemMarginBottom: 5,
            symbolHeight: 7,
            symbolWidth: 7,
            symbolRadius: 7
        }
    });

    function redrawchart4(){
        var chart = $('#container4').highcharts();
        
        console.log('redraw');
        var w = $('#container4').closest(".highcharts-figure-pie4").width()
        // setsize will trigger the graph redraw 
        chart.setSize(       
            w,w * (1.7/4),false
        );
     }
     $(window).on('resize', redrawchart4);
     redrawchart4(); 
}




if(document.querySelector('#container5')){
    Highcharts.chart('container5',{
        chart: {
            type: 'pie',
            height: (6.5 / 16 * 100) + '%', // 16:9 ratio
            style: {
                fontFamily: "Open Sans"
            }
        },
        title:{
            text: null
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<table  border="1" width="50" style="font-size:10px; ">',
            pointFormat: 
                    '<tr>'+
                        '<th style="color:{point.color}"><span style="color:{point.color}">\u25CF</span> <b style="font-weight: 600">{point.name}<b></th>'+
                        '<th></th>'+
                        '<th><b style="font-weight: 600">{point.y}</b></th>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Ignio WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:15px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>25</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Triaging WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:17px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>20</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Manual WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:25px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>15</td>'+
                    '</tr>',
            footerFormat: '</table>',
            // shared: true,
            crosshairs: true,
            useHTML: true,
            outside: true,
            borderWidth:0,
            backgroundColor: 'white'
                
        },
        plotOptions: {
            pie: {
                 // size: 120,
               center: ["10%", "50%"],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            minPointSize: 10,
            innerSize: '60%',
            zMin: 0,
            name: 'countries',
            data: [{
                name: 'Open (60)',
                y: 60,
                z: 92.9
            }, {
                name: 'Resolved (140)',
                y: 140,
                z: 118.7
            }]
        }],
        colors: ['#241F61', '#7C78B5'],
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            itemMarginTop:5,
            itemMarginBottom: 5,
            symbolHeight: 7,
            symbolWidth: 7,
            symbolRadius: 7
        }
    });

    function redrawchart5(){
        var chart = $('#container5').highcharts();
        
        console.log('redraw');
        var w = $('#container5').closest(".highcharts-figure-pie5").width()
        // setsize will trigger the graph redraw 
        chart.setSize(       
            w,w * (1.7/4),false
        );
     }
     $(window).on('resize', redrawchart5);
     redrawchart5(); 
}



if(document.querySelector('#incident-stacked-bar')) {
    Highcharts.chart('incident-stacked-bar', {
        chart: {
            type: 'bar',
            // height: (8 / 16 * 100) + '%', 
            height: '150px',
            title: {
                enabled: false
            },
            style: {
                fontFamily: "Open Sans"
            }
        },
        subtitle: {
            text: ''
                  // text: null // as an alternative
          },
        title: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        colors : ['#D4D3EB', '#7C78B5', '#2A2661'],
        xAxis: {
            categories: ['In-Progress', 'Resolved'],
            minorGridLineWidth: 0,
            gridLineWidth: 0,
        },
        yAxis: {
            min: 0,
            minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
            opposite: false,
            title: {
              enabled: false
            },
        },
        tooltip: {
            // headerFormat: '<b>{point.x}</b><br/>',
            // pointFormat: '{series.name}: {point.y}',
            shared: true,
            outside: true
        },
        plotOptions: {
            bar: {
                stacking: 'normal',
                enabled: true,
                pointPadding: 0,
                groupPadding: 1,
                pointWidth: 20,
                padding: 10,
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        // color: "white",
                        fontSize:'10px',
                        fontWeight: '400',
                        // borderWidth: 0,
                        textOutline: 0
                    }
                },
                states: {
                    hover: {
                        enabled: false
                    }
                }
            }
        },
        series: [{
            name: 'Manual',
            data: [15, 25],
            borderRadiusTopLeft: '4px',
            borderRadiusTopRight: '4px'
        }, {
            name: 'Triage',
            data: [20, 35]
        }, {
            name: 'Ignio',
            data: [25, 80]
        }],
        legend: {
            align: 'right',
            x:10,
            y:20,
            symbolHeight: 7,
            symbolWidth: 7,
            symbolRadius: 7,
            itemStyle: {
                fontWeight: 'normal'
              }
        }
    });
}

if(document.querySelector('#barchart-container-5')) {
    Highcharts.chart('barchart-container-5', {
        chart: {
            type: 'column',
            // height: (3 / 16 * 100) + '%'
            height: '130px',
            style: {
                fontFamily: "Open Sans"
            }
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        colors:['#FBDE6E'],
        credits: {
            enabled: false
        },
        xAxis: {
            type: 'category',
            labels: {
                // rotation: -45,
                style: {
                    fontSize: '13px'
                }
            },
            title: {
                text: 'Issue Name'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>',
            borderWidth:0,
            backgroundColor: 'white'
        },
        series: [{
            name: 'Population',
            data: [
                ['High Drive Utilization', 35],
                ['Heartbeat OK', 74],
                ['High CPU Utilization', 56],
                ['URL Down', 48],
                ['High RAM Utilization', 48]
            ],
            dataLabels: {
                enabled: false,
                // rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
    
}


if(document.querySelector('#barchart-container')) {
    Highcharts.chart('barchart-container', {
        chart: {
            type: 'column',
            height: (3 / 16 * 100) + '%', // 16:9 ratio
            style: {
                fontFamily: "Open Sans"
            }
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        colors:['#FBDE6E'],
        credits: {
            enabled: false
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: 0,
                style: {
                    fontSize: '13px'
                }
            },
            title: {
                text: 'Issue Name'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>',
            borderWidth:0,
            backgroundColor: 'white'
        },
        series: [{
            name: 'Population',
            data: [
                ['High Drive Utilization', 35],
                ['Heartbeat OK', 74],
                ['High CPU Utilization', 56],
                ['URL Down', 48],
                ['High RAM Utilization', 48],
                ['issue-01', 35],
                ['issue-02', 38],
                ['issue-03', 35],
                ['issue-04', 35],
                ['issue-05', 35]
            ],
            dataLabels: {
                enabled: false,
                // rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}



if(document.querySelector('#barchart-container-dow')) {
    Highcharts.chart('barchart-container-dow', {
        chart: {
            type: 'column',
            height: (3 / 16 * 100) + '%', // 16:9 ratio
            style: {
                fontFamily: "Open Sans",
                fontSize: '10px'
            }
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        colors:['#FBDE6E'],
        credits: {
            enabled: false
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: 0,
                style: {
                    fontSize: '19px'
                }
            },
            title: {
                text: 'Day of week',
                y: 12
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>',
            borderWidth:0,
            backgroundColor: 'white'
        },
        plotOptions: {
            series: {
                pointWidth: 55
            }
        },
        series: [{
            name: '',
            data: [
                ['Sun', 10],
                ['Mon', 10],
                ['Tue', 30],
                ['Wed', 20],
                ['Thu', 15],
                ['Fri', 10],
                ['Sat', 15]
            ],
            dataLabels: {
                enabled: false,
                // rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}




// Multibar chart
if(document.querySelector('#multibarchart1')) {
    Highcharts.chart('multibarchart1', {
        chart: {
            type: 'column',
            height: '120px', // 16:9 ratio
            style: {
                fontFamily: "Open Sans"
            }
        },
        title:{
            text: null
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: ['Apples'],
            labels: {
                enabled:false
            },
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            },
            stackLabels: {
                enabled: false
            }
        },
        // legend: {
        //     align: 'right',
        //     // x: -30,
        //     // verticalAlign: 'top',
        //     // y: -100,
        //     verticalAlign: 'bottom',
        //     floating: true,
        //     // floating: true,
        //     backgroundColor:
        //         Highcharts.defaultOptions.legend.backgroundColor || 'white',
        //     borderColor: '#555',
        //     borderWidth: 1,
        //     shadow: false,
        //     outside: true
        // },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
            borderWidth:0,
            backgroundColor: 'white'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            }
        },
        colors: ['#E51722', '#FF832B', '#FFDD33'],
        series: [{
            maxPointWidth: 43,
            name: 'High',
            data: [8]
        }, {
            maxPointWidth: 43,
            name: 'Med',
            data: [2]
        }, {
            maxPointWidth: 43,
            name: 'Low',
            data: [3]
        }],
        legend: {
            // layout: 'vertical',
            align: 'right',
            verticalAlign: 'bottom',
            // itemMarginTop:5,
            // itemMarginBottom: -1,
            floating: true,
            symbolHeight: 7,
            symbolWidth: 7,
            symbolRadius: 7,
            y: 43,
            x: 10,
            outside:true,
            itemStyle: {
                fontWeight: 'normal'
              }
        }
    });
}


// Multibar chart
function multicharts() {
    //high chart
    // var labels = ['0', '50', '100', '150', '200'];
    var data_notes_test = [{
        name: 'Lorem ipsum ',
        color: '#E63730',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x:  6,
        y: 0
    }, {
        name: '<tr>'+
        '<td>Test</td>'+
        '<td>Test</td>'+
        '<td>Test</td>'+
        '</tr>',
        color: '#E63730',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x: 7,
        y: 0
    },
    {
        name: '<tr>'+
        '<td>Test</td>'+
        '<td>Test</td>'+
        '<td>Test</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Test</td>'+
        '<td>Test</td>'+
        '<td>Test</td>'+
        '</tr>',
        color: '#E63730',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x: 8,
        y: 0,
        
    }];

    if(document.querySelector('#multibarchart')) {
      Highcharts.chart('multibarchart', {
          chart: {
              type: 'column',
              height: '130px', // 16:9 ratio
              style: {
                    fontFamily: "Open Sans"
                }
          },
          title:{
              text: null
          },
          credits: {
              enabled: false
          },
          xAxis: {
              categories: ['03/04/20 12:01:34 PM', , , , '04/04/20 12:01:34 PM', , , , '05/04/20 12:01:34 PM'],
            labels: {
                enabled:false
            }
          },
          yAxis: {
              min: 0,
              title: {
                  text: null
              },
              stackLabels: {
                  enabled: false,
              }
          },
        //   legend: {
        //       align: 'right',
        //       x: -30,
        //       verticalAlign: 'top',
        //       y: 25,
        //       floating: true,
        //       backgroundColor:
        //           Highcharts.defaultOptions.legend.backgroundColor || 'white',
        //       borderColor: '#CCC',
        //       borderWidth: 1,
        //       shadow: false
        //   },
          tooltip: {
              headerFormat: '<b>{point.x}</b><br/>',
              pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
              borderWidth:0,
              backgroundColor: 'white'
          },
          plotOptions: {
              column: {
                  stacking: 'normal',
                  dataLabels: {
                      enabled: false
                  }
              }
          },
          colors: ['#FFDD33', '#FF832B' , '#E51722'],
          series: [
          {
              name:'note',
              type:'line',
              data:data_notes_test,
              zIndex: 2,
              shared: false,
              lineWidth: 0,
              dataLabels: {
                  useHTML: true
              }
          },
          {
              name: 'High',
              maxPointWidth: 43,
              data: [50, 40, 20, 60, 50, 70, 30, 50, 40]
          }, {
              name: 'Med',
              maxPointWidth: 43,
              data: [50, 40, 70, 60, 60, 50, 50, 30, 50]
          }, {
              name: 'Low',
              maxPointWidth: 43,
              data: [20, 40, 50, 20, 50, 50, 50, 60, 70]
          }],
          navigation: {
            menuItemStyle: {
                fontSize: '10px'
            }
        },
        legend: {
            enabled: false
        }
      });
  }
  
}

multicharts();


if(document.querySelector('#linechart-container')) {
    var colors = ['#4802D5','#DA3490', '#E8871A' , '#47E26F', '#685FBC', '#81C2DF'];

    var data_notes_test2 = [{
        name: '<tr>'+
        '<td><span class="circle red"><span class="lbl">High</span></td>'+
        '<td><span class="num">150</span></td>'+
        '</tr>'+
        '<tr>'+
        '<td><span class="circle orange"><span class="lbl">Medium</span></td>'+
        '<td><span class="num">175</span></td>'+
        '</tr>'+
        '<tr>'+
        '<td><span class="circle yellow"><span class="lbl">Low</span></td>'+
        '<td><span class="num">75</span></td>'+
        '</tr>',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x: 20.8,
        y: 0
    }, {
        name: '<tr>'+
        '<td><span class="circle red"><span class="lbl">High</span></td>'+
        '<td><span class="num">150</span></td>'+
        '</tr>'+
        '<tr>'+
        '<td><span class="circle orange"><span class="lbl">Medium</span></td>'+
        '<td><span class="num">175</span></td>'+
        '</tr>'+
        '<tr>'+
        '<td><span class="circle yellow"><span class="lbl">Low</span></td>'+
        '<td><span class="num">75</span></td>'+
        '</tr>',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x: 31.1,
        y: 0
    },
    {
        name: '<tr>'+
        '<td><span class="circle red"><span class="lbl">High</span></td>'+
        '<td><span class="num">150</span></td>'+
        '</tr>'+
        '<tr>'+
        '<td><span class="circle orange"><span class="lbl">Medium</span></td>'+
        '<td><span class="num">175</span></td>'+
        '</tr>'+
        '<tr>'+
        '<td><span class="circle yellow"><span class="lbl">Low</span></td>'+
        '<td><span class="num">75</span></td>'+
        '</tr>',
        marker: {
            symbol: 'url(../public/images/icon-warning-charts.svg)'
        },
        x: 41.4,
        y: 0,
        
    }];


Highcharts.chart('linechart-container', {
    chart: {
        type: 'spline',
        height: '155px',
        style: {
            fontFamily: "Open Sans"
        }
    },

    title:{
        text: null
    },
    credits: {
        enabled: false
    },

    title: {
        text: null
    },

    subtitle: {
        text: null
    },

    yAxis: {
        title: {
            text: null
        },
        accessibility: {
            description: null
        }
    },
    tooltip: {
        formatter: function() {
          // this = point
          return this.series.tooltipOptions.customTooltipPerSeries.call(this);
        },
        useHTML: true,
        // shared: true,
        // crosshairs: true,
        borderColor:"#dedede",
        backgroundColor: 'white',
        outside: true
      },
    xAxis: {
        categories: ['03/04/20 12:01:34 PM', null, '04/04/20 12:01:34 PM', null,  '05/04/20 12:01:34 PM'],
        labels: {
            enabled:false
        },
        plotBands: [{
            color: '#FF000024',
            from: 19.4,
            to: 22.1
        },
        {
            color: '#FF000024',
            from: 30.4,
            to: 31.8
        },
        {
            color: '#FF000024',
            from: 40.0,
            to: 42.8
        }]
    },

    // tooltip: {
    //     valueSuffix: '%',
    //     borderWidth:0,
    //     backgroundColor: 'white'
    // },

    plotOptions: {
        spline: {
            lineWidth: 2,
            states: {
                hover: {
                    lineWidth: 2
                }
            },
            marker: {
                enabled: false
            }
        },
        series: {
            point: {
                events: {
                    click: function () {
                        window.location.href = this.series.options.website;
                    }
                }
            },
            cursor: 'pointer'
        }
    },
    series: [
        {
            // name:'note',
            type:'line',
            data:data_notes_test2,
            zIndex: 2,
            shared: false,
            lineWidth: 0,
            dataLabels: {
                useHTML: true
            },
            tooltip: {
                customTooltipPerSeries: function() {
                    return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em><table style="width:100%">'+
                    '<tr>'+
                    '<td><span style="background:#4802D5; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Application</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#DA3490; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Database</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#E8871A; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Middleware</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#47E26F; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">OS</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#685FBC; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Storage</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#81C2DF; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">System</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                '</table>' 
                }
            }
        },
        {
            name: 'NVDA',
            data: [
                3.7, 3.3, 3.9, 5.1, 3.5, 3.8, 4.0, 5.0, 6.1, 3.7, 3.3, 6.4,
                6.9, 6.0, 6.8, 4.4, 4.0, 3.8, 5.0, 4.9, 9.2, 9.6, 9.5, 6.3,
                9.5, 10.8, 14.0, 11.5, 10.0, 10.2, 10.3, 9.4, 8.9, 10.6, 10.5, 11.1,
                10.4, 10.7, 11.3, 10.2, 9.6, 10.2, 11.1, 10.8, 13.0, 12.5, 12.5, 11.3,
                10.1
            ],    
            color: colors[0],
            tooltip: {
                customTooltipPerSeries: function() {
                    return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em><table style="width:100%">'+
                    '<tr>'+
                    '<td><span style="background:#4802D5; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Application</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#DA3490; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Database</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#E8871A; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Middleware</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#47E26F; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">OS</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#685FBC; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Storage</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#81C2DF; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">System</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                '</table>' 
                }
            }
        }, {
            name: 'JAWS',
            data: [6.2, 6.1, 6.1, 6.1, 6.3, 6.2, 6.3, 6.1, 6.7, 6.3, 6.2, 6.2,
                6.3, 6.1, 6.3, 6.8, 6.3, 6.2, 6.3, 6.2, 6.4, 6.6, 6.9, 6.3,
                6.7, 1.1, 1.8, 1.2, 1.4, 1.2, 6.9, 6.8, 6.9, 6.2, 6.4, 1.2,
                6.3, 2.3, 1.6, 6.7, 1.6, 6.8, 2.6, 1.2, 1.4, 3.7, 2.1, 2.6,
                1.5],
            color: colors[1],
            tooltip: {
                customTooltipPerSeries: function() {
                    return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em><table style="width:100%">'+
                    '<tr>'+
                    '<td><span style="background:#4802D5; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Application</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#DA3490; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Database</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#E8871A; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Middleware</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#47E26F; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">OS</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#685FBC; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Storage</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#81C2DF; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">System</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                '</table>' 
                }
            }
        }, {
            name: 'VoiceOver',
            data: [4.7, 4.4, 4.9, 5.1, 4.5, 4.8, 4.0, 7.0, 6.1, 4.7, 4.4, 6.4,
                6.9, 6.0, 6.8, 4.4, 4.0, 3.8, 7.0, 4.9, 9.2, 9.6, 9.7, 6.3,
                9.7, 10.8, 14.0, 11.7, 10.0, 10.2, 10.3, 9.4, 8.9, 10.6, 10.7, 11.1,
                10.4, 10.7, 11.3, 10.2, 9.6, 10.2, 11.1, 10.8, 13.0, 12.7, 12.7, 11.3,
                10.1],
            color: colors[2],
            tooltip: {
                customTooltipPerSeries: function() {
                    return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em><table style="width:100%">'+
                    '<tr>'+
                    '<td><span style="background:#4802D5; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Application</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#DA3490; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Database</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#E8871A; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Middleware</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#47E26F; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">OS</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#685FBC; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Storage</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#81C2DF; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">System</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                '</table>' 
                }
            }
        }, {
            name: 'Narrator',
            data: [
                6.2, 6.1, 6.1, 6.1, 6.3, 6.2, 6.3, 6.1, 6.7, 6.3, 6.2, 6.2,
                6.3, 6.1, 6.3, 6.4, 6.3, 6.2, 6.3, 6.2, 6.4, 6.6, 6.9, 6.3,
                6.7,4.1, 8.8, 11.2, 5.4, 12.2, 6.9, 6.8, 6.9, 6.2, 6.4, 1.2,
                6.3, 2.3, 1.6, 6.7, 1.6, 6.8, 2.6, 1.2, 1.4, 3.7, 2.1, 2.6,
                1.5
            ],
            color: colors[3],
            tooltip: {
                customTooltipPerSeries: function() {
                    return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em><table style="width:100%">'+
                    '<tr>'+
                    '<td><span style="background:#4802D5; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Application</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#DA3490; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Database</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#E8871A; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Middleware</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#47E26F; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">OS</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#685FBC; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Storage</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#81C2DF; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">System</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                '</table>' 
                }
            }
        }, {
            name: 'ZoomText/Fusion',
            data: [
                4.5, 4.3, 4.6, 4.6, 5.3, 6.2, 5.3, 4.1, 6.7, 8.3, 8.2, 8.2,
                8.3, 8.1, 8.3, 8.4, 8.3, 8.2, 8.3, 8.2, 8.4, 8.8, 8.9, 8.3,
                8.7, 11.1, 12.8, 8.2, 3.4, 10.2, 8.9, 8.8, 8.9, 8.2, 8.4, 1.2,
                8.3, 2.3, 1.8, 8.7, 1.8, 8.8, 2.8, 3.2, 8.4, 3.7, 6.1, 2.8,
                1.5
            ],
            color: colors[4],
            tooltip: {
                customTooltipPerSeries: function() {
                    return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em><table style="width:100%">'+
                    '<tr>'+
                    '<td><span style="background:#4802D5; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Application</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#DA3490; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Database</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#E8871A; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Middleware</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#47E26F; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">OS</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#685FBC; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Storage</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#81C2DF; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">System</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                '</table>' 
                }
            }
        }, {
            name: 'Other',
            data: [
                3.7, 8.8, 5.4, 3.4, 9.3, 10.2, 10.3, 10.1, 10.7, 10.3, 10.2, 10.2,
                10.3, 10.1, 10.3, 10.4, 10.3, 10.2, 10.3, 10.2, 10.4, 10.10, 10.9, 10.3,
                10.7, 8.1, 4.8, 1.2, 3.4, 11.2, 10.9, 10.8, 5.8, 10.2, 10.4, 1.2,
                10.3, 2.3, 1.10, 10.7, 1.10, 10.8, 5.10, 6.2, 3.4, 3.7, 9.1, 2.10,
                1.5
            ],
            color: colors[5],
            tooltip: {
                customTooltipPerSeries: function() {
                    return '<em style="font-weight:600; font-size:12px; color:#212121; display:block;  font-style:normal; margin-bottom:1px"><span class="date">05/04/20</span> <span class="title">1:05:31 PM</span></em><table style="width:100%">'+
                    '<tr>'+
                    '<td><span style="background:#4802D5; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Application</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">150</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#DA3490; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Database</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">175</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#E8871A; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Middleware</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#47E26F; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">OS</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#685FBC; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">Storage</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                    '<tr>'+
                    '<td><span style="background:#81C2DF; height:5px; width:5px; border-radius:50%; margin-right:3px; display:inline-block"></span> <span style="font-size:11px; color:#333333;">System</span></td>'+
                    '<td style="text-align:right"><span  style="font-weight:600; color:#212121; font-size:11px">75</span></td>'+
                    '</tr>'+
                '</table>' 
                }
            }
        }
    ],
    navigation: {
        menuItemStyle: {
            fontSize: '10px'
        }
    },
    legend: {
        enabled: false
    },

    responsive: {
        rules: [{
            condition: {
                maxWidth: 550
            },
            chartOptions: {
                chart: {
                    spacingLeft: 3,
                    spacingRight: 3
                },
                legend: {
                    itemWidth: 150
                },
                xAxis: {
                    categories: ['Dec. 2010', 'May 2012', 'Jan. 2014', 'July 2015', 'Oct. 2017', 'Sep. 2019'],
                    title: ''
                },
                yAxis: {
                    visible: false
                }
            }
        }]
    }
});
}


if (document.getElementById('month-heatmap-container')) {
    function getPointCategoryName(point, dimension) {
        var series = point.series,
            isY = dimension === 'y',
            axis = series[isY ? 'yAxis' : 'xAxis'];
        return axis.categories[point[isY ? 'y' : 'x']];
    }

    Highcharts.chart('month-heatmap-container', {

        chart: {
            type: 'heatmap',
            // marginTop: 40,
            // marginBottom: 80,
            plotBorderWidth: 1,
            height: "138px",
            style: {
                fontFamily: "Open Sans"
            }
        },


        title: {
            text: null
        },

        credits: {
            enabled: false
        },

        xAxis: {
            categories: ['Alexander', 'Marie', 'Maximilian', 'Sophia', 'Lukas', 'Maria', 'Leon', 'Anna', 'Tim', 'Laura'],
            labels: {
                enabled:false
            },
            
        },

        yAxis: {
            categories: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
            title: null,
            reversed: true,
            labels: {
                    enabled:false
                },
                
        },

        accessibility: {
            point: {
                descriptionFormatter: function (point) {
                    var ix = point.index + 1,
                        xName = getPointCategoryName(point, 'x'),
                        yName = getPointCategoryName(point, 'y'),
                        val = point.value;
                    return ix + '. ' + xName + ' sales ' + yName + ', ' + val + '.';
                }
            }
        },

        colorAxis: {
            min: 0,
            minColor: '#FFFFFF',
            alternateGridColor: '#FDFFD5',
            maxColor: Highcharts.getOptions().colors[0],
            dataClasses: [
                {
                    from: 1,
                    to: 31,
                    color: '#FFFFFF',
                    name: '0',
                    legendIndex: 5, 
                },
                {
                    from: 2,
                    to: 2,
                    color: '#ffad50',
                    name: ''
                }, {
                    from: 18,
                    to: 18,
                    color: '#ffcf9a',
                    name: ''
                }, {
                    from: 27,
                    to: 27,
                    color: '#ffe6cb',
                    name: '',
                    legendIndex: 3, 
                }, {
                    from: 30,
                    to: 30,
                    color: '#ff9824',
                    name: '4',
                    index:1,
                    legendIndex:0
                }
            ]
        },

        legend: {
            align: 'right',
            layout: 'vertical',
            margin: 0,
            verticalAlign: 'top',
            y: 0,
            marginLeft:10,
            symbolHeight: 18,
            symbolWidth: 10,
            symbolRadius: 0
        },

        tooltip: {
            formatter: function () {
                return '<b>' + getPointCategoryName(this.point, 'x') + '</b> sold <br><b>' +
                    this.point.value + '</b> items on <br><b>' + getPointCategoryName(this.point, 'y') + '</b>';
            }
        },

        series: [{
            borderWidth: 1,
            borderColor: '#ECECEC',
            data: [[0, 0, 1], [0, 1, 8], [0, 2, 15], [0, 3, 22], [0, 4, 29], [1, 0, 2], [1, 1, 9], [1, 2, 16], [1, 3, 23], [1, 4, 30], [2, 0, 3], [2, 1, 10], [2, 2, 17], [2, 3, 24], [2, 4, 31], [3, 0, 4], [3, 1, 11], [3, 2, 18], [3, 3, 25], [4, 0, 5], [4, 1, 12], [4, 2, 19], [4, 3, 26], [5, 0, 6], [5, 1, 13], [5, 2, 20], [5, 3, 27], [6, 0, 7], [6, 1, 14], [6, 2, 21], [6, 3, 28]],
            dataLabels: {
                enabled: true,
                color: '#888888',
                style: {
                    fontSize:'10px',
                    fontWeight: '400',
                    textOutline: 0
                }
            }
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    yAxis: {
                        labels: {
                            formatter: function () {
                                return this.value.charAt(0);
                            }
                        }
                    }
                }
            }]
        }

    });

}





if (document.querySelector('#barchart-container-hod')) {
    Highcharts.chart('barchart-container-hod', {

        chart: {
            type: 'column',
            height: '138',
            marginRight: 60,
            // marginLeft: -10,
            style: {
                fontFamily: "Open Sans",
                fontSize: '10px'
            }
        },
    
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
    
        xAxis: {
            categories: ['07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '01:00', '02:00', '03:00', '04:00', '05:00', '64:00'],
            offset: -50,
            lineWidth: 0,
            tickWidth: 0,
            // title: {
            //     text:"Time"
            // }
            title: {
                align: 'high',
                offset: 0,
                text: 'Time',
                rotation: 0,
                y: 8,
                x: 40
            }
        },
    
        yAxis: [{
            allowDecimals: false,
            title: {
               enabled: false
                
            },
            top: 10,
            height: 40,
            
        },{
            title: {
                text: 'Count',
                x: 0,
                y: -35
            },
            top: 80,
            height: 40,
            offset: 0
        }],
        legend: {
            enabled: false
        },
    
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
    
        plotOptions: {
            column: {
                stacking: 'normal'
            },
            series: {
                pointWidth: 28
            }
        },
    
        series: [{
            name: 'John',
            data: [5, .8, .8, .8, .8, .8, .8, 5, .8, .8, 4, .8],
            color: '#FBDE6E'
        }, {
            name: 'Joe',
            yAxis: 1,
            data: [-0.4, -4, -0.4, -0.4, -0.4 , -5, -0.4, -0.4, -0.4, -0.4, -0.4, -0.4],
            color: '#7C78B5'
        }]
    });
}






// /*---- D3 Charts ----*/
// var graph = {
//     "nodes": [
//         {
//             "id": 1,
//             "type": "TimeSheet",
//             "name": "TimeSheet",
//             "context": [
//                 {
//                     "name": "TimeSheet"
//                 }
//             ],
//             "icon": "\uf1ad"
//         },
//         {
//             "id": 2,
//             "type": "ts-appsvr-1",
//             "name": "ts-appsvr-1",
//             "context": [
//                 {
//                     "name": "ts-appsvr-1"
//                 }
//             ],
//             "icon": "\uf7b1",
//             "parent": 1
//         },
//         {
//             "id": 3,
//             "type": "ts-appsvr-1-1",
//             "name": "ts-appsvr-1-1",
//             "context": [
//                 {
//                     "name": "ts-appsvr-1-1"
//                 }
//             ],
//             "icon": "\uf78d",
//             "parent": 1
//         },
//         {
//             "id": 4,
//             "type": "ts-ora-prod",
//             "name": "ts-ora-prod",
//             "context": [
//                 {
//                     "name": "ts-ora-prod"
//                 }
//             ],
//             "icon": "\ue084",
//             "parent": 1
//         },
//         {
//             "id": 5,
//             "type": "software",
//             "name": "Software_4",
//             "context": [
//                 {
//                     "name": "Software_4"
//                 }
//             ],
//             "icon": "\ue084",
//             "parent": 1
//         },
//         {
//             "id": 6,
//             "type": "software",
//             "name": "Software_5",
//             "context": [
//                 {
//                     "name": "Software_5"
//                 }
//             ],
//             "icon": "\ue084",
//             "parent": 4
//         },
//         {
//             "id": 7,
//             "type": "software",
//             "name": "Software_6",
//             "context": [
//                 {
//                     "name": "Software_6"
//                 }
//             ],
//             "icon": "\ue084",
//             "parent": 4
//         },
//         {
//             "id": 8,
//             "type": "software",
//             "name": "Software_5",
//             "context": [
//                 {
//                     "name": "Software_5"
//                 }
//             ],
//             "icon": "\ue084",
//             "parent": 5
//         },
//         {
//             "id": 9,
//             "type": "software",
//             "name": "Software_6",
//             "context": [
//                 {
//                     "name": "Software_6"
//                 }
//             ],
//             "icon": "\ue084",
//             "parent": 5
//         },
//         {
//             "id": 10,
//             "type": "software",
//             "name": "Software_7",
//             "context": [
//                 {
//                     "name": "Software_7"
//                 }
//             ],
//             "icon": "\ue084",
//             "parent": 5
//         },
//          {
//             "id": 11,
//             "type": "software",
//             "name": "Software_8",
//             "context": [
//                 {
//                     "name": "Software_8"
//                 }
//             ],
//             "icon": "\ue084",
//             "parent": 5
//         },
//          {
//             "id": 12,
//             "type": "software",
//             "name": "Software_9",
//             "context": [
//                 {
//                     "name": "Software_9"
//                 }
//             ],
//             "icon": "\ue084",
//             "parent": 5
//         },
//          {
//             "id": 13,
//             "type": "software",
//             "name": "Software_10",
//             "context": [
//                 {
//                     "name": "Software_10"
//                 }
//             ],
//             "icon": "\ue084",
//             "parent": 5
//         }
//     ],
//     "links": [
//         {
//             "source": 2,
//             "target": 1,
//             "type": "Timesheet"
//         },
//         {
//             "source": 3,
//             "target": 1,
//             "type": "ts-appsvr-1"
//         },
//         {
//             "source": 5,
//             "target": 2,
//             "type": "ts-appsvr-1-1"
//         },
//         {
//             "source": 6,
//             "target": 2,
//             "type": "ts-appsvr-1-1"
//         },
//         {
//             "source": 7,
//             "target": 5,
//             "type": "ts-appsvr-1-1"
//         },
//         {
//             "source": 8,
//             "target": 5,
//             "type": "ts-appsvr-1-1"
//         },
//         {
//             "source": 9,
//             "target": 5,
//             "type": "ts-appsvr-1-1"
//         },
//         {
//             "source": 3,
//             "target": 4,
//             "type": "ts-appsvr-1-1"
//         },
//         {
//             "source": 4,
//             "target": 2,
//             "type": "ts-appsvr-1-1"
//         },
//         {
//             "source": 10,
//             "target": 3,
//             "type": "ts-appsvr-1-1"
//         },
//        {
//             "source": 11,
//             "target": 4,
//             "type": "uses"
//         },
//         {
//             "source": 12,
//             "target": 11,
//             "type": "uses"
//         },
//        {
//             "source": 13,
//             "target": 12,
//             "type": "uses"
//         }
//     ]
// }

 var width = 664,
     height = 276,
     root;
var config = {
    "avatar_size": 100//define the size of teh circle radius
}
// var force = d3.layout.force()
//     .linkDistance(80)
//     .charge(-120)
//     .gravity(.05)
//     .size([width, height])
//     .on("tick", tick);

// var svg = d3.select("#networkcontainer").append("svg")
//     .attr("width", width)
//     .attr("height", height);


// var defs = svg.append("defs");
// var marker = defs.append('marker')
//     .attr('id', 'arrow')
//     .attr("viewBox", "0 -5 10 10")
//     .attr('markerWidth', 5)
//     .attr('markerHeight', 5)
//     .attr('refX', 35)
//     // .attr('refY', '3')
//     .attr('orient', 'auto')
//     .attr('markerUnits', 'strokeWidth');
// var mpath = marker.append('path').attr('d', "M0,-5L10,0L0,5").attr('fill', '#8189ad');

// // var linksContainer = svg.append("g").attr("class", "linksContainer")
// // var nodesContainer = svg.append("g").attr("class", "nodesContainer")

// var link = svg.selectAll(".link"),
//     node = svg.selectAll(".node");

// d3.json("public/images/graph.json", function (error, json) {
//     if (error) throw error;

//     root = json;
//     update();
// });

// function update(d, i) {
//     var nodes = flatten(root),
//         links = d3.layout.tree().links(nodes);

//     // Restart the force layout.
//     force
//         .nodes(nodes)
//         .links(links)
//         .start();

//     // Update links.
//     link = link.data(links, function (d) { return d.target.id; });

//     link.exit().remove();

//     link.enter().insert("line", ".node")
//         .attr("class", "link").attr("marker-end", "url(#arrow)");

//     // Update nodes.
//     node = node.data(nodes, function (d) { return d.id; });

//     node.exit().remove();

//     var nodeEnter = node.enter().append("g")
//         .attr("class", function (d) { return d.class })
//         .on("click", click)
//         .on("load", click1)
//         .call(force.drag);

//     nodeEnter.append("circle")
//         //   .attr("r", function(d) { return Math.sqrt(d.size) / 5 || 4.5; });
//         .attr("r", '17');

//     nodeEnter.append("text")
//         .attr("dy", ".35em").attr('transform', "translate(0,29)")
//         .text(function (d) { return d.name; });



//     nodeEnter.append('image').attr('href', function (d) { return d.img }).attr('transform', "translate(-8,-8)");


//     node.select("circle")
//         .style("fill", function (d) { return d.fill }).style('stroke-width', '9px').style('stroke', function (d) { return d.stroke });
// }

// function tick() {
//     link.attr("x1", function (d) { return d.source.x; })
//         .attr("y1", function (d) { return d.source.y; })
//         .attr("x2", function (d) { return d.target.x; })
//         .attr("y2", function (d) { return d.target.y; });

//     node.attr("transform", function (d) { return "translate(" + d.x + "," + d.y + ")"; });


// }

// // function color(d) {
// //   return d._children ? "#ffdd33" // collapsed package
// //       : d.children ? "#5cb85c" // expanded package
// //       : "#e51722"; // leaf node
// // }

// // function strokecolor(d) {
// //     return d._children ? "rgb(255 221 51 / 55%)" // collapsed package
// //         : d.children ? "rgb(92 184 92 / 55%)" // expanded package
// //         : "rgb(229 23 34 / 55%)"; // leaf node
// //   }

// // Toggle children on click.


// function click1(d) {
//     node.exit().remove();
// }

// window.onload = function (d) {
//     click1(d)
// }

// function click(d) {
//     if (d3.event.defaultPrevented) return; // ignore drag
//     if (d.children) {
//         d._children = d.children;
//         d.children = null;
//     } else {
//         d.children = d._children;
//         d._children = null;
//     }
//     update();
// }

// // Returns a list of all nodes under the root.
// function flatten(root) {
    
//     var nodes = [], i = 0;

//     function recurse(node) {
//         if (node.children) node.children.forEach(recurse);
//         if (!node.id) node.id = ++i;
//         nodes.push(node);
//     }

//     recurse(root);
//     return nodes;
// }








if (document.querySelector('#map1container')) {
    // var svg = d3.select("#map1container").find(".datamap")
    // .attr("width", width)
    // .attr("height", height);


    var bubble_map = new Datamap({
        element: document.getElementById('map1container'),
        scope: 'india',
        geographyConfig: {
            popupOnHover: true,
            // highlightOnHover: true,
            borderColor: '#d6e8f0',
            borderWidth: 0.5,
            dataUrl: 'https://rawgit.com/Anujarya300/bubble_maps/master/data/geography-data/india.topo.json'
            //dataJson: topoJsonData
        },
        fills: {
            'MAJOR': '#ffdd33',
            'Irhel': '#5cb85c',
            'MEDIUM': '#5cb85c',
            'MINOR': '#FA1A1A',
            defaultFill: '#eff8f8'
        },
        imgfills: {
            'img1': 'public/images/icon-network-apps.svg',
            'img2': 'public/images/icon-network-database.svg',
            'img3': 'public/images/icon-network-appserver.svg',
            'img4': 'public/images/icon-network-rhel.svg',
            defaultImgFill: 'public/images/icon-network-rhel.svg'
        },
        // data: {
        //     'JH': { fillKey: 'MINOR' },
        //     'MH': { fillKey: 'MINOR' }
        // },
        setProjection: function (element) {
            var projection = d3.geo.mercator()
                .center([78.9629, 23.5937]) // always in [East Latitude, North Longitude]
                .scale(3500);
            var path = d3.geo.path().projection(projection);
            return { path: path, projection: projection };
        }
    });

    let bubbles = [
        {
            centered: "MH",
            fillKey: "Irhel",
            imgfillKey: "img4",
            radius: 23,
            subtitle: "RHEL",
            state: "Maharastra"
        },
        {
            centered: "TS",
            fillKey: "MAJOR",
            imgfillKey: "img3",
            radius: 23,
            subtitle: "App",
            state: "Telangana"
        },
        {
            centered: "CT",
            fillKey: "MAJOR",
            imgfillKey: "img1",
            radius: 23,
            subtitle: "App Server",
            state: "Chhattisgarh"
        },
        {
            centered: "OD",
            fillKey: "MINOR",
            imgfillKey: "img2",
            radius: 23,
            subtitle: "Database",
            state: "Odisha"
        },
        {
            centered: "AP",
            fillKey: "MINOR",
            imgfillKey: "img4",
            radius: 23,
            subtitle: "App",
            state: "Andhra Pradesh"
        }

    ]

    let data = {

    };
    // // ISO ID code for city or <state></state>
    setTimeout(() => { // only start drawing bubbles on the map when map has rendered completely.
        bubble_map.bubbles(bubbles, {
            popupTemplate: function (geo, data) {
                // Code for tooltip indicator 
                return `
                <div class="vl"></div>
                <div class="hoverinfo">${data.state}<br> (${data.subtitle})</div>
                `;
            }
        });

        document.querySelector('#map1container svg').setAttribute("width", width)
        document.querySelector('#map1container svg').setAttribute("height", height)
        document.querySelector('#map1container svg').setAttribute("viewBox", '0 0 1200 552')
        document.querySelector('#map1container svg').setAttribute("xmlns", 'http://www.w3.org/2000/svg')
        document.querySelector('#map1container svg').setAttribute("version", '1.1')

    }, 1000);
}


if(document.querySelector('#eventstacked-bar')) {
    Highcharts.chart('eventstacked-bar', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Stacked bar chart'
        },
        xAxis: {
            categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total fruit consumption'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'John',
            data: [5, 3, 4, 7, 2]
        }, {
            name: 'Jane',
            data: [2, 2, 3, 2, 1]
        }, {
            name: 'Joe',
            data: [3, 4, 4, 2, 5]
        }]
    });
}


if(document.querySelector('#event-stacked-bar')){
    Highcharts.chart('event-stacked-bar', {
        chart: {
            type: 'bar',
            height: 24,
            spacingBottom: 0,
            spacingTop: 0,
            spacingLeft: 0,
            spacingRight: 0,
            marginBottom: 0,
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
        },
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: ['1', '2', '3', '4', '5'],
             labels: {
               enabled:false
             },
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            },
          labels: {
            enabled:false
          }
        },
        legend: {
            /* reversed: true,
            align: 'right', */
            enabled: false
        //    enabled: true,
        //    align: 'right',
        //    x:10,
        //    y:20,
        //    symbolHeight: 7,
        //    symbolWidth: 7,
        //    symbolRadius: 7,
        //    itemStyle: {
        //        fontWeight: 'normal'
        //      },
        //     labelFormatter: function () {
        //         return {
        //             '1.5K' : 'Suppressed',
        //             '1.2K': 'Aggregated',
        //             '850': 'Actionable'
        //         }[this.name];
        //     }
        },
        stackLabels: {
            qTotals: ['This','is','a','qTotal','!'],
            enabled: false,
            style: {
                fontWeight: 'bold'
               // color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            },
            formatter: function() {            
                return this.options.qTotals[this.x];
            }
        },
        tooltip: {
            headerFormat: '<table  border="1" width="50" style="font-size:10px; ">',
            pointFormat: 
                    '<tr>'+
                        '<th style="color:{point.color}"><span style="color:{point.color}">\u25CF</span> <b style="font-weight: 600">{point.dtLabel}<b></th>'+
                        '<th></th>'+
                        '<th><b style="font-weight: 600">{point.y}</b></th>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Ignio WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:15px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>25</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Triaging WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:17px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>20</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Manual WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:25px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>15</td>'+
                    '</tr>',
            footerFormat: '</table>',
            outside: true,
            shadow: false,
            backgroundColor: 'white',
            borderColor: "#dedede",
            useHTML: true
        },
        plotOptions: {
            series: {
              fillOpacity: 0.1,
              stacking: 'percent',
              dataLabels: {
                  enabled: true,
                  format: "{series.name}",
                  style: {
                    fontSize: '12px',
                    color:'white',
                    fontWeight: '600',
                    fill: 'none',
                    textOutline: "none"
                }
              }
            },
           bar: {
             pointWidth: 24
           },
          
          },
          
          colors: ['#0761B4', '#67B6E5', '#A2E3F3'],
          series: [{
              name: '850',
              data: [{y:2,dtLabel:'Actionable'}],
              borderRadiusTopLeft: '4px',
              borderRadiusTopRight: '4px',
          }, {
              name: '1.2K',
              data: [{y:3,dtLabel:'Aggregated'}]
          }, {
              name: '1.5K',
              data: [{y:5,dtLabel:'Suppressed'}],
              borderRadiusBottomRight: '4px',
              borderRadiusBottomLeft: '4px',
          }]
    });
}

if(document.querySelector('#event-present-stacked-bar')){
    
Highcharts.chart('event-present-stacked-bar', {
    chart: {
        type: 'bar',
        height: 24,
        spacingBottom: 0,
        spacingTop: 0,
        spacingLeft: 0,
        spacingRight: 0,
        marginBottom: 0,
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
    },
    title: {
        text: null
    },
    credits: {
    	enabled: false
    },
    xAxis: {
        categories: ['1', '2', '3', '4', '5'],
         labels: {
           enabled:false
         },
    },
    yAxis: {
        min: 0,
        title: {
            text: null
        },
      labels: {
        enabled:false
      }
    },
    legend: {
        /* reversed: true,
        align: 'right', */
        enabled: false
    },
    stackLabels: {
        qTotals: ['This','is','a','qTotal','!'],
        enabled: false,
        style: {
            fontWeight: 'bold'
           // color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
        },
        formatter: function() {            
            return this.options.qTotals[this.x];
        }
    },
    tooltip: {
        headerFormat: '<table  border="1" width="50" style="font-size:10px; ">',
        pointFormat: 
                '<tr>'+
                    '<th style="color:{point.color}"><span style="color:{point.color}">\u25CF</span> <b style="font-weight: 600">{point.dtLabel}<b></th>'+
                    '<th></th>'+
                    '<th><b style="font-weight: 600">{point.y}</b></th>'+
                '</tr>'+
                '<tr>'+
                    '<td>Ignio WIP</td>'+
                    '<td style="width:20px"><span style="background-color:{point.color}; width:15px; height:10px; display:block; border-radius:3px;"></span></td>'+
                    '<td>25</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>Triaging WIP</td>'+
                    '<td style="width:20px"><span style="background-color:{point.color}; width:17px; height:10px; display:block; border-radius:3px;"></span></td>'+
                    '<td>20</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>Manual WIP</td>'+
                    '<td style="width:20px"><span style="background-color:{point.color}; width:25px; height:10px; display:block; border-radius:3px;"></span></td>'+
                    '<td>15</td>'+
                '</tr>',
        footerFormat: '</table>',
        outside: true,
        shadow: false,
        backgroundColor: 'white',
        borderColor: "#dedede",
        useHTML: true
    },
     plotOptions: {
      series: {
        fillOpacity: 0.1,
        stacking: 'percent',
        dataLabels: {
            enabled: true,
            format: "{series.name}",
            style: {
              fontSize: '12px',
              color:'white',
              fontWeight: '600',
              fill: 'none',
              textOutline: "none"
          }
        }
      },
     bar: {
       pointWidth: 24
     },
    
    },
    colors: ['#0761B4', '#67B6E5', '#A2E3F3'],
    series: [{
        name: '200',
        data: [{y:2,dtLabel:'Actionable'}],
        borderRadiusTopLeft: '4px',
        borderRadiusTopRight: '4px',
    }, {
        name: '300',
        data: [{y:3,dtLabel:'Aggregated'}]
    }, {
        name: '500',
        data: [{y:5,dtLabel:'Suppressed'}],
        borderRadiusBottomRight: '4px',
        borderRadiusBottomLeft: '4px',
    }]
});
}

if(document.querySelector('#incident-past-stacked-bar')){
    Highcharts.chart('incident-past-stacked-bar', {
        chart: {
            type: 'bar',
            height: 24,
            spacingBottom: 0,
            spacingTop: 0,
            spacingLeft: 0,
            spacingRight: 0,
            marginBottom: 0,
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
        },
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: ['1', '2', '3', '4', '5'],
             labels: {
               enabled:false
             },
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            },
          labels: {
            enabled:false
          }
        },
        legend: {
            /* reversed: true,
            align: 'right', */
            enabled: false
        },
        stackLabels: {
            qTotals: ['This','is','a','qTotal','!'],
            enabled: false,
            style: {
                fontWeight: 'bold'
               // color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            },
            formatter: function() {            
                return this.options.qTotals[this.x];
            }
        },
        tooltip: {
            headerFormat: '<table  border="1" width="50" style="font-size:10px; ">',
            pointFormat: 
                    '<tr>'+
                        '<th style="color:{point.color}"><span style="color:{point.color}">\u25CF</span> <b style="font-weight: 600">{point.dtLabel}<b></th>'+
                        '<th></th>'+
                        '<th><b style="font-weight: 600">{point.y}</b></th>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Ignio WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:15px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>25</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Triaging WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:17px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>20</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Manual WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:25px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>15</td>'+
                    '</tr>',
            footerFormat: '</table>',
            outside: true,
            shadow: false,
            backgroundColor: 'white',
            borderColor: "#dedede",
            useHTML: true
        },
         plotOptions: {
          series: {
            fillOpacity: 0.1,
            stacking: 'percent',
            dataLabels: {
                enabled: true,
                format: "{series.name}",
                style: {
                  fontSize: '12px',
                  color:'white',
                  fontWeight: '600',
                  fill: 'none',
                  textOutline: "none"
              }
            }
          },
         bar: {
           pointWidth: 24
         },
        
        },
        colors: ['#7534DD', '#BE95FF'],
        series: [{
            name: '80',
            data: [{y:3,dtLabel:'Open'}],
            borderRadiusTopLeft: '4px',
            borderRadiusTopRight: '4px',
        },{
            name: '770',
            data: [{y:5,dtLabel:'Resolved'}],
            borderRadiusBottomRight: '4px',
            borderRadiusBottomLeft: '4px',
        }]
    });
}

if(document.querySelector('#incident-present-stacked-bar')){
    Highcharts.chart('incident-present-stacked-bar', {
        chart: {
            type: 'bar',
            height: 24,
            spacingBottom: 0,
            spacingTop: 0,
            spacingLeft: 0,
            spacingRight: 0,
            marginBottom: 0,
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
        },
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: ['1', '2', '3', '4', '5'],
             labels: {
               enabled:false
             },
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            },
          labels: {
            enabled:false
          }
        },
        legend: {
            /* reversed: true,
            align: 'right', */
            enabled: false
        },
        stackLabels: {
            qTotals: ['This','is','a','qTotal','!'],
            enabled: false,
            style: {
                fontWeight: 'bold'
               // color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            },
            formatter: function() {            
                return this.options.qTotals[this.x];
            }
        },
        tooltip: {
            headerFormat: '<table  border="1" width="50" style="font-size:10px; ">',
            pointFormat: 
                    '<tr>'+
                        '<th style="color:{point.color}"><span style="color:{point.color}">\u25CF</span> <b style="font-weight: 600">{point.dtLabel}<b></th>'+
                        '<th></th>'+
                        '<th><b style="font-weight: 600">{point.y}</b></th>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Ignio WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:15px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>25</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Triaging WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:17px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>20</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Manual WIP</td>'+
                        '<td style="width:20px"><span style="background-color:{point.color}; width:25px; height:10px; display:block; border-radius:3px;"></span></td>'+
                        '<td>15</td>'+
                    '</tr>',
            footerFormat: '</table>',
            outside: true,
            shadow: false,
            backgroundColor: 'white',
            borderColor: "#dedede",
            useHTML: true
        },
         plotOptions: {
          series: {
            fillOpacity: 0.1,
            stacking: 'percent',
            dataLabels: {
                enabled: true,
                format: "{series.name}",
                style: {
                  fontSize: '12px',
                  color:'white',
                  fontWeight: '600',
                  fill: 'none',
                  textOutline: "none"
              }
            }
          },
         bar: {
           pointWidth: 24
         },
        
        },
        colors: ['#7534DD', '#BE95FF'],
        series: [{
            name: '140',
            data: [{y:3,dtLabel:'Open'}],
            borderRadiusTopLeft: '4px',
            borderRadiusTopRight: '4px',
        },{
            name: '60',
            data: [{y:3,dtLabel:'Resolved'}],
            borderRadiusBottomRight: '4px',
            borderRadiusBottomLeft: '4px',
        }]
    });
}

